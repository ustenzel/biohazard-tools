{-# LANGUAGE ExistentialQuantification, Rank2Types, TemplateHaskell #-}
{- This is now supposed to read both Ensembl- and UCSC-style input, and
 - it had a very weird heuristic to convert them into each other.  We
 - drop this shit now, the rules are:  following the specification, all
 - coordinates are 0-based half open intervals for BED, BAM,
 - weird-FastA, 5col; 1-based closed intervals for SAM.  Annotations in
 - 5col format (and only in 5col format!) can be rewritten using a
 - translation table.  If the coordinate conventions change, the offsets
 - in the translation table need to reflect that.
 -
 - Strand information is optional now, too.  We handle it this way:
 -
 - * If the strand field starts with '-', only the reverse strand is meant.
 - * If the strand field is "0" or absent, both strands are meant.
 - * Else only the forward strand is meant.
 - * Every annotation is reported at most once.
 -
 - Internally, we *always* use zero-based, half-open intervals.  This is
 - also the assumption for the 5col format, except when translation
 - from Emsembl to UCSC is requested.  Since Ensembl uses one-based,
 - closed intervals, we'll follow that convention.  If that is
 - confusing, just don't use stupid home grown fole formats. -}

import Bio.Prelude
import Bio.Streaming
import Bio.Util.Git
import Control.Concurrent.Async              ( Async, async, link, wait )
import Control.Monad.Log
import Control.Monad.Trans.Reader            ( ReaderT(..), ask )
import Control.Monad.Trans.State.Strict      ( evalStateT, execStateT, runStateT, get, put )
import Data.ByteString.Builder               ( toLazyByteString )
import Options.Applicative
import Paths_biohazard_tools                 ( version )

import qualified Bio.Streaming.Prelude              as Q
import qualified Data.HashMap.Strict                as M
import qualified Data.HashSet                       as S
import qualified Data.IntervalMap                   as I
import qualified Network.Socket                     as N
import qualified Network.Socket.ByteString.Lazy     as N ( sendAll )

import Expound.FormattedIO
import Expound.Network
import Expound.Symtab

data AnnoFormat = AnnoBed | Anno5Col

readAnno :: MonadLog m => AnnoFormat -> FilePath -> Maybe ChromTable -> ByteStream m r -> Stream (Of Region) m r
readAnno AnnoBed   _  _ s  =  readMyBed s
readAnno Anno5Col fp ct s  =  lift (logMsg Warning $ LegacyWarning fp) >> read5Col ct s

data LegacyWarning = LegacyWarning FilePath deriving (Typeable, Show)
instance Exception LegacyWarning where
    displayException (LegacyWarning fp) =
        "The five column format is not a standard format.  Consider\
        \ converting " ++ fp ++ " to the standard BED format."


data Options = Options
     { optOutput      :: Output
     , optAnnotations :: [( AnnoFormat, FilePath )]
     , optChromTable  :: Maybe FilePath
     , optWhich       :: Which
     , optXForm       :: Region -> Region
     , optNetAddr     :: Maybe (String, String)
     , optFiles       :: [FilePath] }

options :: Parser Options
options = Options
    <$> (outputTable <$> strOption
            (short 'o' <> long "output" <> metavar "FILE" <> help "write output to FILE") <|>
         outputPattern <$> strOption
            (short 'O' <> long "output-pattern" <> metavar "PAT" <> help "write outp'ut to files following PAT") <|>
         flag' outputSummary (long "summarize" <> help "only summarize annotations to stdout") <|>
         pure (outputTable "-"))

    <*> many ((,) AnnoBed <$> strOption
                (short 'a' <> long "annotation" <> metavar "FILE" <> help "read annotations from FILE in Bed format") <|>
              (,) Anno5Col <$> strOption
                (short 'A' <> long "legacy-annotation" <> metavar "FILE" <>
                 help "read annotations from FILE in legacy 5c format"))
    <*> optional (strOption
            (short 'c' <> long "chroms" <> metavar "FILE" <> help "read chr translation table from FILE"))
    <*> (flag' Covering  (long "covered"   <> help "output only annotations that cover the whole query interval") <|>
         flag' Covered   (long "contained" <> help "output only annotations contained in the query interval") <|>
         flag' Fragment  (long "partial"   <> help "output only annotations that cover only part of the query") <|>
         flag' Inclusive (long "inclusive" <> help "output annotations that overlap at least part of the query") <|>
         flag' Nearest   (long "nearest"   <> help "output the closest annotation if none overlaps") <|>
         pure Inclusive)
    <*> flag id eraseStrand (short 's' <> long "nostrand" <> help "ignore strand information in region files")
    <*> optional ((,)
         <$> strOption (short 'H' <> long "host" <> metavar "HOST" <> help "connect to HOST")
         <*> strOption (short 'p' <> long "port" <> metavar "PORT" <> help "connect to PORT"))
    <*> many (strArgument (metavar "FILE"))


options' :: Parser (Either String Options)
options' =
    Left  <$> strOption (short 'l' <> long "listen" <> metavar "PORT" <> help "Start server on port PORT") <|>
    Right <$> options


nubs :: (a -> [Gene]) -> [a] -> [Gene]
nubs _ [ ]  =  []
nubs f [x]  =  f x
nubs f  xs  =  S.toList . S.fromList . concatMap f $ xs

type LookupFn = Word -> Word -> [I.IntervalMap Gene] -> AnnoSet

standardLookup :: (Word -> Word -> I.IntervalMap Gene -> [Gene]) -> LookupFn
standardLookup which s e = Hits . nubs (which s e)

lookupNearest :: LookupFn
lookupNearest s e tabs = case (leftmiss, rightmiss) of
    _ | not (null hits)                            ->  Hits hits
    (Nothing,           Nothing)               ->  Hits [  ]
    (Just (_,b,v),      Nothing)               ->  NearMiss v (I.w2i $ s-b)
    (Nothing,      Just (c,_,w))               ->  NearMiss w (I.w2i $ c-e)
    (Just (_,b,v), Just (c,_,w)) | s-b <= c-e  ->  NearMiss v (I.w2i $ s-b)
                                 | otherwise   ->  NearMiss w (I.w2i $ c-e)
  where
    hits      = nubs (I.lookupOverlap s e) tabs
    leftmiss  = foldl' I.rightmost Nothing $ mapMaybe (I.lookupLeft  s) tabs
    rightmiss = foldl'  I.leftmost Nothing $ mapMaybe (I.lookupRight e) tabs


interpretWhich :: Which -> LookupFn
interpretWhich Covered   = standardLookup I.lookupContained
interpretWhich Covering  = standardLookup I.lookupContaining
interpretWhich Fragment  = standardLookup I.lookupPartial
interpretWhich Inclusive = standardLookup I.lookupOverlap
interpretWhich Nearest   = lookupNearest


main :: IO ()
main =
    execWithParser_ options' (Just version) (giFullVersion $$gitInfo)
                    (header "Annotate genomic regions" <>
                     progDesc "Annotates genomic regions given by coordinates.  Input files\
                              \ and annotation files are BED or BAM files.  Can operate in\
                              \ client-server-mode." <> fullDesc) $
      \case Left  port -> run_server port
            Right opts -> maybe (run_standalone opts) (run_client opts) (optNetAddr opts)

run_standalone :: Options -> LIO ()
run_standalone Options{..} = do
    ct <- forM optChromTable (flip streamFile readChromTable)

    _ :!: anno <- streamInputs (map snd optAnnotations)
                        $ execAnnoM M.empty M.empty
                        . readGeneTable
                        . progressNum "reading annotations" 16384
                        . concats
                        . maps (\(Compose ((tp,fp) :> s)) -> readAnno tp fp ct s)
                        . zips (Q.each optAnnotations)

    let annotate :: Region -> (Bytes, AnnoSet)
        annotate (Region name c senses s e) =
            (,) name $ interpretWhich optWhich (I.i2w s) (I.i2w e) $
            withSenses senses $ \sense -> M.lookupDefault I.empty (sense c) anno

    case optOutput of
        Output hdr iter summ ->
            let go file = iter file . Q.map (annotate . optXForm)
            in hdr >> myReadFiles optFiles go >>= summ


myReadFiles :: [FilePath] -> (forall r . FilePath -> Stream (Of Region) LIO r -> LIO (Of b r)) -> LIO [b]
myReadFiles fs k = streamInputs fs $
    Q.toList_ .
    mapped (\(Compose (nm :> s)) -> k nm . progressNum nm 16384 $ readInput s) .
    zips (Q.each fs)

asyncLIO :: LIO r -> LIO (Async r)
asyncLIO (Logged k) = Logged $ ReaderT $ \env -> async (runReaderT k env)

forkLIO :: LIO r -> LIO ()
forkLIO (Logged k) = Logged $ ReaderT $ \env -> void $ forkIO (void $ runReaderT k env)

data LookupFailed = LookupFailed String String deriving (Typeable, Show)
instance Exception LookupFailed where
    displayException (LookupFailed h p) = "Lookup of " ++ h ++ ":" ++ p ++ "failed."

run_client :: Options -> (N.HostName, N.ServiceName) -> LIO ()
run_client Options{..} (h,p) = do
    let hints = N.defaultHints { N.addrFlags = [N.AI_ADDRCONFIG, N.AI_CANONNAME] }
    liftIO (N.getAddrInfo (Just hints) (Just h) (Just p)) >>= \case
      [    ] -> liftIO $ throwIO $ LookupFailed h p
      addr:_ -> do
        ct <- forM optChromTable (flip streamFile readChromTable)
        sock   <- liftIO $ N.socket (N.addrFamily addr) (N.addrSocketType addr) (N.addrProtocol addr)
        liftIO $ N.connect sock (N.addrAddress addr)

        -- request/upload each annotation set
        buf' <- execStateT (runReaderT (forM_ optAnnotations $ \(annotype, fp) ->
                                                streamInput fp $
                                                sendGeneTable fp .
                                                progressNum ("uploading " ++ fp) 16384 .
                                                readAnno annotype fp ct)
                           sock) mempty

        -- fork thread to upload all the input
        uploader <- asyncLIO $ do void $ myReadFiles optFiles $ \file strm -> do
                                       liftIO $ N.sendAll sock . toLazyByteString . putRequest $ StartFile (fromString file)
                                       (() :>) <$> flip Q.mapM_ strm
                                                       ( liftIO . N.sendAll sock . toLazyByteString . putRequest
                                                       . Anno optWhich . optXForm )
                                  liftIO $ N.sendAll sock . toLazyByteString $ putRequest EndStream
        liftIO $ link uploader

        -- receive responses, print output
        case optOutput of
            Output hdr iter summ -> do
                hdr
                us <- evalStateT (runReaderT (myReadNet iter) sock) buf'
                summ us

        liftIO $ void $ wait uploader
        liftIO $ N.close sock

-- | Asks the server for an annotation set, and uploads it if it isn't
-- already present.
sendGeneTable :: MonadIO m => FilePath -> Stream (Of Region) (Client m) () -> Client m ()
sendGeneTable fp strm = do
    send . putRequest $ StartAnno (fromString fp)
    resp <- receive getResponse
    case resp of
        KnownAnno   -> return ()
        UnknownAnno -> do Q.mapM_ (send . putRequest . AddAnno) strm
                          send $ putRequest EndAnno
        _           -> liftIO $ throwIO ProtoError

data ProtoError = ProtoError deriving (Typeable, Show)
instance Exception ProtoError where
    displayException _ = "unexpected network message"

-- | Reads from network and passes it on, calling the continuation
-- once for each transmitted \"file\".
myReadNet :: MonadIO m => (forall b . FilePath -> Stream (Of Annotation) m b -> m (Of u b)) -> Client m [u]
myReadNet k = receive getResponse >>= \case
                 StartedFile nm -> readAll [] (Just nm)
                 EndedStream    -> return []
                 _              -> liftIO $ throwIO ProtoError
  where
    readAll acc  Nothing  = return $ reverse acc
    readAll acc (Just nm) = do sock <- ask
                               buf <- lift get
                               u :> (b,w) <- lift . lift $ k (unpack nm) (thread sock buf read1)
                               lift (put $! w)
                               readAll (u:acc) b

    read1 :: MonadIO m => Stream (Of Annotation) (Client m) (Maybe Bytes)
    read1 = lift (receive getResponse) >>= \case
                Result nm annos -> Q.cons (nm,annos) read1
                StartedFile nm  -> pure (Just nm)
                EndedStream     -> pure Nothing
                _               -> liftIO $ throwIO ProtoError

    thread :: MonadIO m => N.Socket -> Bytes -> Stream (Of a) (Client m) r -> Stream (Of a) m (r, Bytes)
    thread sock buf as = lift (runStateT (runReaderT (inspect as) sock) buf) >>= \case
        (Left     r,     buf') -> pure (r, buf')
        (Right (a:>as'), buf') -> wrap (a :> thread sock buf' as')


type HalfTable = ( Bytes, Annotab, Symtab )
type FullTable = ( Annotab, Symtab )
type FullTables = M.HashMap Bytes FullTable

run_server :: N.ServiceName -> LIO ()
run_server svname = do
    full_tables <- liftIO $ newMVar M.empty

    let hints = N.defaultHints { N.addrFlags = [N.AI_PASSIVE], N.addrSocketType = N.Stream }
    liftIO (N.getAddrInfo (Just hints) Nothing (Just svname)) >>= \case
      [  ] -> liftIO . throwIO $ LookupFailed "" svname
      ai:_ -> do
        listener <- liftIO $ N.socket (N.addrFamily ai) (N.addrSocketType ai) (N.addrProtocol ai)
        liftIO $ N.bind listener $ N.addrAddress ai
        liftIO $ N.listen listener 1
        logStringLn $ "waiting for connections on " ++ show (N.addrAddress ai)
        forever $ do (sk, addr) <- liftIO $ N.accept listener
                     logStringLn $ "connection from " ++ show addr
                     forkLIO $ service getRequest putResponse (serverfn (shows addr ": ") full_tables) sk (Nothing,[])

serverfn :: String
         -> MVar FullTables
         -> ( Maybe HalfTable, [FullTable] )
         -> Request
         -> LIO ( ( Maybe HalfTable, [FullTable] ), Maybe Response)
serverfn  _ _fulltables (Just  _,   _) (StartAnno    _) = liftIO $ throwIO ProtoError
serverfn msg fulltables (Nothing, fts) (StartAnno name) = do
    tablemap <- liftIO $ readMVar fulltables
    case M.lookup name tablemap of
        Just m  -> do logStringLn $ msg ++ "brought known annotation " ++ shows name " in scope"
                      return ( (Nothing, m:fts), Just KnownAnno)
        Nothing -> do logStringLn $ msg ++ "starting new annotation set " ++ show name
                      return ( (Just (name, M.empty, M.empty), fts), Just UnknownAnno )

serverfn _ _ st (StartFile nm) = return ( st, Just $ StartedFile nm )
serverfn _ _ st  EndStream     = return ( st, Just   EndedStream )

serverfn _ _ (Nothing,_) (AddAnno _) = liftIO $ throwIO ProtoError
serverfn _ _ (Just (name, annotab, symtab), st) (AddAnno (Region gi chrom senses s e)) = do
    symtab' :!: annotab' <- execAnnoM symtab annotab $ do
                                sym <- intern gi
                                sequence_ $ withSenses senses $ \sense ->
                                    annoChrom (sense chrom) s e sym
    pure $ ((Just (name, annotab', symtab'), st), Nothing)

serverfn _ _ (Nothing,_) EndAnno = liftIO $ throwIO ProtoError
serverfn msg fulltables (Just (name, annotab, symtab), s) EndAnno = do
    let anno = (annotab, symtab)
    tablemap <- liftIO $ takeMVar fulltables
    liftIO $ putMVar fulltables $! M.insert name anno tablemap
    logStringLn $ msg ++ "memoized new annotation set " ++ show name
    return ((Nothing, anno:s), Nothing)

serverfn _ _ st@(_,annosets) (Anno which (Region name c senses s e)) = do
    let result = Result name . foldr combine_anno_sets (Hits []) $
           [ interpretWhich which (I.i2w s) (I.i2w e) (withSenses senses $ \sense -> M.lookupDefault I.empty (sense c) anno)
           | (anno, _) <- annosets ]
    return (st, Just result)

combine_anno_sets :: AnnoSet -> AnnoSet -> AnnoSet
combine_anno_sets (Hits []) x = x
combine_anno_sets x (Hits []) = x
combine_anno_sets (Hits xs) (NearMiss _ _) = Hits xs
combine_anno_sets (NearMiss _ _) (Hits ys) = Hits ys
combine_anno_sets (Hits xs) (Hits ys)      = Hits (xs++ys)
combine_anno_sets (NearMiss a d) (NearMiss b d')
    | abs d <= abs d' = NearMiss a d
    | otherwise       = NearMiss b d'


