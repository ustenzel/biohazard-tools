{-# LANGUAGE DeriveFunctor, DeriveFoldable, TemplateHaskell #-}
import Bio.Bam
import Bio.Bam.Evan                 ( removeWarts )
import Bio.Prelude
import Bio.Util.Git
import Options.Applicative
import Paths_biohazard_tools        ( version )

import qualified Bio.Streaming.Bytes        as S
import qualified Bio.Streaming.Prelude      as Q
import qualified Data.ByteString            as B
import qualified Data.ByteString.Char8      as C
import qualified Data.Vector.Generic        as V

type OFn = BamMeta -> Stream (Of BamRec) LIO () -> LIO ExitCode

data Opts = Opts { output  :: OFn
                 , inputs  :: [Input]
                 , quant   :: Bool
                 , merge   :: Maybe (Int,Int) }

data Input = Input { _read1  :: FilePath         -- ^ file with first read (or other stuff)
                   ,  read2  :: Maybe FilePath   -- ^ optional file with second read
                   , index1  :: Maybe FilePath   -- ^ optional file with first index
                   , index2  :: Maybe FilePath   -- ^ optional file with second index
                   , lindex1 :: Int }            -- ^ length of first index contained in first read
  deriving Show

plainInput :: FilePath -> Input
plainInput fn = Input fn Nothing Nothing Nothing 0

ooptions :: Parser OFn
ooptions = do_write <$> strOption (short 'o' <> long "output" <> help "Write output to FILE" <> metavar "FILE")
       <|> option disabled (short 'X' <> long "exec" <> metavar "COMMAND" <> help "Send FastQ output to a program")
       <|> pure do_pipe
  where
    do_write f h s = ExitSuccess <$ writeBamFile f h s
    do_pipe h s = ExitSuccess <$ pipeBamOutput h (protectTerm s)

options :: Parser OFn -> Parser Opts
options oopts =
    Opts <$> oopts
         <*> (foldl (&) [] <$> many inputOption)
         <*> switch (short 'Q' <> long "quantize" <> help "Quantize quality scores")
         <*> (flag' (Just (20,200)) (short 'm' <> long "fuse" <> long "fuse-default") <|>
              option (Just <$> read_two) (short 'f' <> long "fuse-overlap" <> metavar "QLOW,QHIGH" <>
                                          help "Attempt to merge or trim reads") <|>
              pure Nothing)
  where
    read_two = str >>= \s ->
                    case [ (a,b) | ( a ,s1) <- reads s
                                 , (",",s2) <- lex s1
                                 , ( b ,"") <- reads s2 ] of
                        [r] -> pure r
                        _   -> readerError $ "cannot parse `" ++ s ++ "'"

inputOption :: Parser ([Input] -> [Input])
inputOption =
    (\fn -> (plainInput fn :)) <$> strArgument (metavar "FILE") <|>
    (\fn -> (plainInput fn :)) <$> strOption
        (short '1' <> long "read-one" <> help "Parse FILE for anything" <> metavar "FILE") <|>
    (\fn -> at_head (\i -> i { read2  = Just fn })) <$> strOption
        (short '2' <> long "read-two" <> help "Parse FILE for second mates" <> metavar "FILE") <|>
    (\fn -> at_head (\i -> i { index1 = Just fn })) <$> strOption
        (short 'I' <> long "index-one" <> help "Parse FILE for first index" <> metavar "FILE") <|>
    (\fn -> at_head (\i -> i { index2 = Just fn })) <$> strOption
        (short 'J' <> long "index-two" <> help "Parse FILE for second index" <> metavar "FILE") <|>
    (\n  -> at_head (\i -> i { lindex1 = n})) <$> option auto
        (short 'l' <> long "length-index-one" <> metavar "NUM" <> help "Read 1 ends in NUM index bases")
  where
    at_head f [    ] = [ f $ plainInput "-" ]
    at_head f (i:is) = f i : is


pipe_to :: FilePath -> [String] -> BamMeta -> Stream (Of BamRec) LIO () -> LIO ExitCode
pipe_to cmd args _ = pipeToCmd cmd args . Q.foldrT conv
  where
    conv br s = liftIO (formatFastq br) >>= flip Q.cons s

main :: IO ()
main = do (args,cmd) <- break (`elem` ["-X","--exec"]) `fmap` getArgs
          exitWith =<< case cmd of
              _:cmd':args' -> withArgs args $ main' (pure (pipe_to cmd' args'))
              _            -> main' ooptions

main' :: Parser OFn -> IO ExitCode
main' oopts =
    execWithParser_ (options oopts) (Just version) (giFullVersion $$gitInfo)
                       (fullDesc <> header "Read multiple FastA or FastQ files and convert them to BAM"
                                 <> progDesc "See manpage for details.") $ \conf -> do

          let eff_inputs = if null (inputs conf) then [ plainInput "-" ] else reverse (inputs conf)
          mapM_ (logStringLn . show) eff_inputs
          pgm <- addPG (Just version)

          withADSeqs default_fwd_adapters $ \fads ->
               withADSeqs default_rev_adapters $ \rads ->
                    output conf (pgm mempty)
                    . bool id (Q.map quantize) (quant conf)
                    . maybe Q.concat ((Q.concat .) . Q.mapM . uncurry (mergeDuals fads rads)) (merge conf)
                    . progress
                    $ mapM_ enum_input eff_inputs


data UpToTwo a = One a | Two a a deriving (Functor, Foldable)

getOne :: UpToTwo a -> a
getOne (One a)   = a
getOne (Two a _) = a

mergeDuals :: MonadIO m => AD_Seqs -> AD_Seqs -> Int -> Int -> UpToTwo BamRec -> m [BamRec]
mergeDuals fads rads lowq highq (Two r1 r2) = liftIO $ mergeBam lowq highq fads rads r1 r2
mergeDuals fads    _ lowq highq (One    r1) = liftIO $ trimBam  lowq highq fads r1

fromFastq :: (MonadIO m, MonadLog m) => FilePath -> Q.Stream (Of BamRec) m ()
fromFastq fp
    | fp == "-" = go stdin
    | otherwise = do h <- liftIO $ openBinaryFile fp ReadMode
                     go h <* liftIO (hClose h)
  where
    go = Q.map removeWarts . parseFastq . S.gunzip . S.hGetContents

enum_input :: (MonadIO m, MonadLog m) => Input -> Q.Stream (Q.Of (UpToTwo BamRec)) m ()
enum_input (Input r1 mr2 mi1 mi2 il1) =
    Q.map (addIdx il1) $
    withIndex mi1 "XI" "YI" $
    withIndex mi2 "XJ" "YJ" $
    maybe (Q.map One $ fromFastq r1) (enumDual r1) mr2

addIdx :: Int -> UpToTwo BamRec -> UpToTwo BamRec
addIdx 0 brs = brs
addIdx l brs = r
  where
    br1 :> r = case brs of One a   -> a :> One (doext br1')
                           Two a b -> a :> Two (doext br1') (doext b)

    l' = V.length (b_seq br1) - l
    br1'     = br1 { b_seq  = V.take l' $ b_seq br1, b_qual = V.take l' <$> b_qual br1 }
    doext br = br  { b_exts = xi $ yi $ b_exts br }

    xi = updateE "XI" . Text . C.pack . map showNucleotides . V.toList . V.drop l' $ b_seq  br1
    yi = maybe id (updateE "YI" . Text . B.pack . map ((+) 33 . unQ)  . V.toList . V.drop l') (b_qual br1)


-- | Given a 'Q.Stream' and maybe a filename, reads index sequences from
-- the file and merges them with the 'Q.Stream'.  In case of mismatched
-- names, a warning is produced and the index is ignored.  The
-- streams may later resynchronize.
withIndex :: (MonadIO m, MonadLog m)
          => Maybe FilePath -> BamKey -> BamKey
          -> Stream (Of (UpToTwo BamRec)) m () -> Stream (Of (UpToTwo BamRec)) m ()
withIndex  Nothing     _    _ strm = strm
withIndex (Just fp) tagi tagq strm = combine True strm (fromFastq fp)
  where
    combine w seqs idxs = do
      lift ((,) <$> inspect seqs <*> inspect idxs) >>= \case
        (Left    _,    Left _) -> pure ()
        (Right (x:>s), Left _) -> lift (logMsg Error $ MismatchedNames (b_qname (getOne x)) mempty) >> Q.cons x s
        (Left _, Right (y:>_)) -> lift (logMsg Error $ MismatchedNames mempty (b_qname y))
        (Right (seqrecs:>s), Right (idxrec:>t))
            | b_qname (getOne seqrecs) == b_qname idxrec
                -> Q.cons (fmap upd seqrecs) (combine True s t)
            | otherwise
                -> do when w . lift . logMsg Error $ MismatchedNames (b_qname (getOne seqrecs)) (b_qname idxrec)
                      Q.cons seqrecs $ combine False s (Q.cons idxrec t)
          where
            idxseq  = C.pack . map showNucleotides . V.toList  $  b_seq idxrec
            idxqual = B.pack . map   ((+33) . unQ) . V.toList <$> b_qual idxrec
            upd   r = r { b_exts = maybe id (insertE tagq . Text) idxqual $
                                   insertE tagi (Text idxseq) $ b_exts r }


data MismatchedNames = MismatchedNames Bytes Bytes deriving (Typeable, Show)

instance Exception MismatchedNames where
    displayException (MismatchedNames a b) =
        "read names do not match: " ++ show1 a ++ " / " ++ show1 b
      where
        show1 x = if B.null x then "<EOF>" else show x


-- Enumerate dual files.  We read two FastQ files and match them up.  We
-- must make sure the names match, and we will flag everything as
-- 1st/2nd mate, no matter if the syntactic warts were present in the
-- files themselves.
enumDual :: (MonadIO m, MonadLog m) => FilePath -> FilePath -> Q.Stream (Q.Of (UpToTwo BamRec)) m ()
enumDual f1 f2 = combine True (fromFastq f1) (fromFastq f2)
  where
    combine w strm1 strm2 =
      lift ((,) <$> inspect strm1 <*> inspect strm2) >>= \case
        (Left    _,    Left _) -> pure ()
        (Right (x:>s), Left _) -> lift (logMsg Error $ MismatchedNames (b_qname x) mempty) >> Q.map One (Q.cons x s)
        (Left _, Right (y:>_)) -> lift (logMsg Error $ MismatchedNames mempty (b_qname y))
        (Right (fstMt:>s), Right (sndMt:>t))
            | b_qname fstMt == b_qname sndMt
                -> Q.cons (Two fstMt' sndMt') (combine True s t)
            | otherwise
                -> do when w . lift . logMsg Error $ MismatchedNames (b_qname fstMt) (b_qname sndMt)
                      Q.cons (One fstMt) $ combine False s (Q.cons sndMt t)
              where
                qc = (b_flag fstMt .|. b_flag sndMt) .&. flagFailsQC
                addx k = maybe id (updateE k) $ lookup k (b_exts fstMt) <|> lookup k (b_exts sndMt)
                add_indexes = addx "XI" . addx "XJ" . addx "YI" . addx "YJ"

                fstMt' = fstMt { b_flag = qc.|. flagFirstMate.|.flagPaired.|.b_flag fstMt .&. complement flagSecondMate
                               , b_exts = add_indexes $ b_exts fstMt }
                sndMt' = sndMt { b_flag = qc.|.flagSecondMate.|.flagPaired.|.b_flag sndMt .&. complement flagFirstMate
                               , b_exts = add_indexes $ b_exts sndMt }


progress :: MonadLog m => Stream (Of (UpToTwo BamRec)) m b -> Stream (Of (UpToTwo BamRec)) m b
progress = go 0 0
  where
    go   !l !n          = lift . inspect >=> either (fin n) (step l (succ n))

    step !l !n (a :> s) = do
        let !nm = b_qname $ getOne a
            !l' = l `max` C.length nm
        when (n .&. 0x1fff == 0) $ lift $ do
            logString_ $ printf "%*s, %d records processed" l' (C.unpack nm) (n::Int)
        Q.cons a $ go l' n s

    fin !n r = do
        lift $ logString_ []
        lift $ logStringLn $ printf "%d records processed." (n::Int)
        pure r

quantize :: BamRec -> BamRec
quantize br = br { b_qual = V.map f <$> b_qual br }
  where
    f (Q q) | q  >=  40 = Q 40
            | q  >=  35 = Q 37
            | q  >=  30 = Q 33
            | q  >=  25 = Q 27
            | q  >=  20 = Q 22
            | q  >=  10 = Q 15
            | q  >=   2 = Q  6
            | otherwise = Q  0


