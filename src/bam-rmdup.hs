{-# LANGUAGE TemplateHaskell #-}
import Bio.Bam
import Bio.Bam.Rmdup
import Bio.Prelude
import Bio.Streaming.Furrow                     ( afford, drain, evertStream )
import Bio.Util.Git
import Data.PriorityQueue.External
import Options.Applicative
import Paths_biohazard_tools                    ( version )

import qualified Bio.Streaming.Prelude          as Q
import qualified Data.ByteString.Char8          as S
import qualified Data.HashMap.Strict            as M
import qualified Data.IntMap                    as I
import qualified Data.Vector.Generic            as V
import qualified Data.Vector.Unboxed            as U ( Vector )
import qualified Data.Vector.Unboxed.Mutable    as U ( length, replicate, read, write )

data Conf = Conf
    { output           :: Maybe ((BamRec -> Bytes) -> BamMeta -> Stream (Of BamRec) LIO () -> LIO ())
    , put_result       :: String -> IO ()
    , strand_preserved :: Bool
    , collapse         :: Bool -> Collapse LIO
    , allow_multi      :: Bool
    , keep_all         :: Bool
    , keep_unaligned   :: Bool
    , keep_improper    :: Bool
    , transform        :: BamRec -> Maybe BamRec
    , min_len          :: Int
    , min_qual         :: Qual
    , get_label        :: HashMap Bytes Bytes -> BamRec -> Bytes
    , which            :: Which
    , circulars        :: [( Bytes, Int )]
    , pqconf           :: PQ_Conf LIO
    , input            :: [FilePath] }

-- | Which reference sequences to scan
data Which = Allrefs | Some Refseq Refseq | Unaln deriving (Show, Eq)

options :: Parser Conf
options = uncurry Conf
    <$> ((\fp -> (Just (const (writeBamFile fp)), if fp == "-" then hPutStr stderr else putStr)) <$> strOption
            (short 'o' <> long "options" <> metavar "FILE" <> help "Write to FILE (default: no output, count only)") <|>
         (\fp -> (Just (writeLibBamFiles fp), putStr)) <$> strOption
            (short 'O' <> long "output-lib" <> metavar "PAT" <> help "Write each lib to file named following PAT") <|>
         flag' (Just (const pipeSamOutput), hPutStr stderr)
            (long "debug" <> help "Write textual debugging output") <|>
         pure (Nothing, putStr))
    <*> flag True False
            (short 's' <> long "no-strand" <> help "Strand of alignments is uninformative")
    <*> (cons_collapse' . Q <$> option auto
            (short 'Q' <> long "max-qual" <> metavar "QUAL" <> help "Set maximum quality after consensus call to QUAL") <|>
         flag' cheap_collapse'
            (short 'c' <> long "cheap" <> help "Cheap computation: skip the consensus calling") <|>
         pure (cons_collapse' (Q 60)))
    <*> switch (short 'm' <> long "multimappers" <> help "Process multi-mappers (by dropping secondary alignments)")
    <*> switch (short 'k' <> long "keep" <> long "mark-only" <> help "Mark duplicates, but include them in output")
    <*> switch (short 'u' <> long "unaligned" <> help "Include unaligned reads and pairs")
    <*> switch (short 'p' <> long "improper-pairs" <> help "Include improper pairs")
    <*> flag Just make_single (short '1' <> long "single-read" <> help "Pretend there is no second mate")
    <*> option auto (value 0 <>
             short 'l' <> long "min-length" <> metavar "LEN" <> help "Discard reads shorter than LEN")
    <*> (Q <$> option auto (value 0 <>
             short 'q' <> long "min-mapq" <> metavar "QUAL" <> help "Discard reads with map quality lower than QUAL"))
    <*> flag get_library get_no_library
            (short 'r' <> long "ignore-rg" <> help "Ignore read groups when looking for duplicates")
    <*> option (eitherReader parse_range) (value Allrefs <>
             short 'R' <> long "refseq" <> metavar "RANGE" <> help "Read only range of reference sequences")
    <*> many (option (eitherReader parse_circ)
            (short 'z' <> long "circular" <> metavar "CHR:LEN" <> help "Refseq CHR is circular with length LEN"))
    <*> (PQ_Conf
        <$> option auto (value 1000 <>
                 short 'M' <> long "max-memory" <> metavar "MB" <> help "Use at most MB megabytes per queue")
        <*> option auto (value 200 <>
                 short 'L' <> long "max-merge" <> metavar "NUM" <> help "Merge at most NUM files at a time")
        <*> strOption (value "/var/tmp/" <>
                 short 'T' <> long "temp-path" <> metavar "PATH" <> help "Store temporary files in PATH")
        <*> pure logStringLn)
    <*> many (strArgument (metavar "FILE"))


parse_range :: String -> Either String Which
parse_range "A" = Right Allrefs
parse_range "a" = Right Allrefs
parse_range "U" = Right Unaln
parse_range "u" = Right Unaln
parse_range  a  = case reads a of
        [ (x,"")    ]  -> Right $ Some (Refseq $ x-1) (Refseq $ x-1)
        [ (x,'-':b) ] -> case reads b of
            [ (y,"") ] -> Right $ Some (Refseq $ x-1) (Refseq $ y-1)
            _ -> err
        _ -> err
  where
    err = Left $ "parse error in " ++ show a

parse_circ :: String -> Either String (Bytes,Int)
parse_circ a = case break (':' ==) a of
        (nm,':':r) -> case reads r of
            [(l,[])] | l > 0 -> Right (fromString nm,l)
            _ -> Left $ "couldn't parse length " ++ show r ++ " for " ++ show nm
        _ -> Left $ "couldn't parse \"circular\" argument " ++ show a


-- Takes the original list of references, return the \"circularization
-- table\" and the new list of references.  The circularization table
-- maps the indices of circular sequences to a triple of their name,
-- their true length, and their original length in the input file.
app_circulars :: Refs -> [(Bytes,Int)] -> LIO (IntMap (Bytes,Int,Int), Refs)
app_circulars (Refs refs) xs = do v <- liftIO $ thawArray refs 0 (sizeofArray refs)
                                  go v I.empty xs
  where
    go v m [         ] = (,) m . Refs <$> liftIO (unsafeFreezeArray v)
    go v m ((nm,l):ss) =
        case filter (S.isPrefixOf nm . sq_name . snd) $ zip [0..] $ toList refs of
            [(k,a)] | sq_length a >= l -> do liftIO (writeArray v k a { sq_length = l })
                                             go v (I.insert k (sq_name a, l, sq_length a) m) ss
                    | otherwise        -> logMsg Error (TooLong nm l (sq_length a))              >> go v m ss
            [     ]                    -> logMsg Error (NoMatch nm)                              >> go v m ss
            as                         -> logMsg Error (ManyMatches nm (map (sq_name . snd) as)) >> go v m ss


data TooLong = TooLong !Bytes !Int !Int deriving (Typeable, Show)
instance Exception TooLong where
    displayException (TooLong nm l la) =
        printf "cannot wrap %s to %d, which is more than the original %d" (show nm) l la

data NoMatch = NoMatch !Bytes deriving (Typeable, Show)
instance Exception NoMatch where
    displayException (NoMatch nm) =
        "no match for target sequence " ++ unpack nm

data ManyMatches = ManyMatches !Bytes [Bytes] deriving (Typeable, Show)
instance Exception ManyMatches where
    displayException (ManyMatches nm nms) =
        "target sequence " ++ unpack nm ++ " is ambiguous, it matches "
        ++ intercalate ", " (map unpack nms)


cons_collapse' :: Qual -> Bool -> Collapse LIO
cons_collapse' m False = cons_collapse m
cons_collapse' m True  = cons_collapse_keep m

cheap_collapse' :: Bool -> Collapse LIO
cheap_collapse'  False = cheap_collapse
cheap_collapse'  True  = cheap_collapse_keep

-- | Get library from BAM record.
-- This gets the read group from a bam record, then the library for read
-- group.  This will work correctly if and only if the RG-LB field is
-- the name of the "Ur-Library", the common one before the first
-- amplification.
--
-- If no RG-LB field is present, RG-SM is used instead.  This will work
-- if and only if no libraries were aliquotted and then pooled again.
--
-- Else the RG-ID field is used.  This will work if and only if read
-- groups correspond directly to libraries.
--
-- If no RG is present, the empty string is returned.  This serves as
-- fall-back.

get_library, get_no_library :: M.HashMap Bytes Bytes -> BamRec -> Bytes
get_library  tbl br = M.lookupDefault rg rg tbl where rg = extAsString "RG" br
get_no_library _  _ = S.empty

mk_rg_tbl :: BamMeta -> M.HashMap Bytes Bytes
mk_rg_tbl hdr = M.fromList
    [ (rg_id, rg_lb)
    | ("RG",fields) <- meta_other_shit hdr
    , rg_id <- take 1   [ i | ("ID",i) <- fields ]
    , rg_lb <- take 1 $ [ l | ("LB",l) <- fields ]
                     ++ [ s | ("SM",s) <- fields ]
                     ++ [ rg_id ] ]

-- The stored vectors shall never be empty, and they shall never contain all zeros.
data Counts v = Counts { tin          :: !Int
                       , tout         :: !Int
                       , good_singles :: !Int
                       , good_total   :: !Int
                       , histogram    :: v Int }

type Stats = M.HashMap Bytes (Counts U.Vector)


main :: IO ()
main =
  execWithParser_ options (Just version) (giFullVersion $$gitInfo)
    (progDesc "Removes duplicates from BAM files and calls a consensus for each duplicate set. \
              \Input files must be sorted by coordinate and are merged on the fly." <>
     header "Removes duplicates from BAM files" <> fullDesc) $ \Conf{..} -> do

    let pqconf1 = pqconf { max_mb = 1 +      max_mb pqconf `div` 16 }
        pqconf2 = pqconf { max_mb = 1 + 15 * max_mb pqconf `div` 16 }

    add_pg <- addPG $ Just version
    mergeInputRanges which input $ \hdr stream -> do
        (circtable, refs) <- app_circulars (meta_refs hdr) circulars
        let !tbl = mk_rg_tbl hdr

        unless (M.null tbl) $ do
                 logStringLn "mapping of read groups to libraries:"
                 mapM_ logStringLn [ unpack k ++ " --> " ++ unpack v | (k,v) <- M.toList tbl ]

        let cleanup :: BamRaw -> LIO (Maybe BamRec)
            cleanup br = do let b = transform $ unpackBam br
                            unless allow_multi $ mapM_ check_multi_flags b
                            pure . mfilter is_nice $ clean_multi_flags =<< b

            is_nice :: BamRec -> Bool
            is_nice b = and [ keep_unaligned || is_aligned b
                            , keep_improper || is_proper b
                            , eff_len b >= min_len ]

            mywrap (_nm,ln,_lp) = map (either id id) . wrapTo ln
            wrap_output o = o (get_label tbl) (add_pg hdr { meta_refs = refs }) .
                            mapSortAtGroups pqconf2 Destroyed circtable mywrap

        maybe Q.effects wrap_output output $ do
            ct :> us <- Q.map snd .
                        Q.store count_all .
                        ( let cc = maybe cheap_collapse' (const collapse) output
                          in rmdup strand_preserved (cc keep_all) ) .
                        Q.map (\b -> (get_label tbl b, b)) .
                        Q.filter (\b -> b_mapq b >= min_qual) .
                        ( let norm (nm,lnat,lpad) r = [ normalizeFwd nm lnat lpad r ]
                          in mapSortAtGroups pqconf1 Preserved circtable norm ) .
                        Q.concat . Q.mapM cleanup .
                        progressPos coordinates "Rmdup at" refs 0x8000 .
                        Q.span is_halfway_aligned $
                        stream

            when (keep_unaligned || which == Unaln) .
                Q.concat . Q.mapM cleanup .
                progressPos coordinates "Copying junk at" refs 0x8000 $ us

            liftIO . put_result . unlines
                   $ "#RG\tin\tout\tin@MQ20\tsingle@MQ20\tunseen\ttotal\t%unique\t%exhausted\thistogram"
                   : map (uncurry do_report) (M.toList ct)


do_report :: Bytes -> Counts U.Vector -> String
do_report lbl Counts{..} = intercalate "\t" fs
  where
    fs = label : showNum tin : showNum tout : showNum good_total : showNum good_singles :
         report_estimate (estimateComplexity good_total good_singles :: Maybe Double) ++
         [intercalate "," (map show (V.toList histogram))]

    label = if S.null lbl then "--" else unpack lbl

    report_estimate  Nothing                = [ "N/A", "", "", "" ]
    report_estimate (Just good_grand_total) =
            [ showOOM (grand_total - fromIntegral tout)
            , showOOM grand_total
            , showFFloat (Just 1) rate []
            , showFFloat (Just 1) exhaustion [] ]
      where
        grand_total = good_grand_total * fromIntegral tout / fromIntegral good_total
        exhaustion  = 100 * fromIntegral good_total / good_grand_total
        rate        = 100 * fromIntegral tout / fromIntegral tin :: Double


-- | Counting reads:  we count total read in (ti), total reads out (to),
-- good (confidently mapped) singletons out (gs), total good
-- (confidently mapped) reads out (gt).  Paired reads count 1, unpaired
-- reads count 2, and at the end we divide by 2.  This ensures that we
-- don't double count mate pairs, while still working mostly sensibly in
-- the presence of broken BAM files.

count_all :: PrimMonad m => Stream (Of ((Bytes,Int),BamRec)) m r -> m (Of Stats r)
count_all = Q.foldM plus (pure M.empty) (mapM fixup)
  where
    plus !m ((l,i),b) =
        maybe (Counts 0 0 0 0 <$> U.replicate 1024 0) pure (M.lookup l m) >>=
        fmap (\cs -> M.insert l cs m) . plus1 i b

    plus1 i b (Counts ti to gs gt v) = do
        let !w = if isPaired b then 1 else 2
        when (i >= 1 && i <= U.length v) $ U.read v (i-1) >>= U.write v (i-1) . (+) w
        pure $! Counts (ti + w * extAsInt 1 "XP" b)
                       (to + w)
                       (if b_mapq b >= Q 20 && extAsInt 1 "XP" b == 1 then gs + w else gs)
                       (if b_mapq b >= Q 20                           then gt + w else gt)
                       v

    -- The histograms have a nasty tendency to end in lots of zeros,
    -- followed by a spurious one.  Nobody wants to see that.  On the
    -- other hand, we can get zeros at the beginning, because someone
    -- decided to sequence until the cows came home.  If so, we should
    -- keep them.  To clean it up, we truncate the histogram before the
    -- first zero entry above the mean.  (Fortunately, by construction,
    -- the mean is always defined.)
    fixup (Counts ti to gs gt v) = do
        vv <- V.unsafeFreeze v
        let m1 = V.ifoldl' (\a i x -> a + i*x) 0 vv
            (lo,hi) = V.splitAt (m1 `div` V.sum vv) vv
        pure $ Counts (div ti 2) (div to 2) (div gs 2) (div gt 2)
                      (V.map (`div` 2) $ lo V.++ V.takeWhile (/= 0) hi)



eff_len :: BamRec -> Int
eff_len b | isProperlyPaired b = abs $ b_isize b
          | otherwise          = V.length $ b_seq b

is_halfway_aligned :: BamRaw -> Bool
is_halfway_aligned = isValidRefseq . b_rname . unpackBam

is_aligned :: BamRec -> Bool
is_aligned b = not (isUnmapped b && isMateUnmapped b) && isValidRefseq (b_rname b)

is_proper :: BamRec -> Bool
is_proper b = not (isPaired b) || (isMateUnmapped b == isUnmapped b && isProperlyPaired b)

make_single :: BamRec -> Maybe BamRec
make_single b | isPaired b && isSecondMate b = Nothing
              | isUnmapped b                 = Nothing
              | not (isPaired b)             = Just b
              | otherwise = Just $ b { b_flag = b_flag b .&. complement pair_flags
                                     , b_mrnm = invalidRefseq
                                     , b_mpos = invalidPos
                                     , b_isize = 0 }
  where
    pair_flags = flagPaired .|. flagProperlyPaired .|.
                 flagFirstMate .|. flagSecondMate .|.
                 flagMateUnmapped


mergeInputRanges :: (MonadIO m, MonadLog m, MonadMask m)
                 => Which -> [FilePath] -> (BamMeta -> Stream (Of BamRaw) m () -> m r) -> m r
mergeInputRanges Allrefs = mergeInputsOn coordinates
mergeInputRanges     rng = \fps k -> decodeRanges fps $ \bs -> do
        let hdr = foldMap fst bs
        sequence_ $ zipWith (\f (h,_) -> guardRefCompat ("*",hdr) (f,h)) fps bs
        k hdr (foldr (\a b -> void $ mergeStreamsOn coordinates (snd a) b) (pure ()) bs)
  where
    decodeRanges [    ] k = k []
    decodeRanges (f:fs) k = decode1  rng f  $ \h bs ->
                            decodeRanges fs $ \bss ->
                            k $ (h,bs) : bss

    decode1  Allrefs   f k = decodeBamFile f k
    decode1  Unaln     f k = withIndexedBam f $ \hdr idx hdl -> k hdr $ streamBamUnaligned idx hdl
    decode1 (Some x y) f k = withIndexedBam f $ \hdr idx hdl -> k hdr $ mapM_ (streamBamRefseq idx hdl) [x..y]



writeLibBamFiles :: (MonadIO m, MonadMask m) => FilePath -> (BamRec -> Bytes) -> BamMeta -> Stream (Of BamRec) m r -> m r
writeLibBamFiles fp lbl hdr s0 =
    bracket (liftIO $ newIORef [])
            (liftIO . readIORef >=> liftIO . mapM_ hClose)
            (\hs -> go hs M.empty s0)
  where
    go hs !m = inspect >=> \case
        Left          r -> mapM_ drain m >> return r
        Right (br :> s) -> do
            -- Note:  We can't use 'writeBamFile', which would
            -- automatically close all 'Handle's, here, because that
            -- would require an @instance MonadMask (Stream f m)@, which
            -- cannot be provided.  Instead, we collect the 'Handle's in
            -- an 'IORef' and close them in a 'bracket' around the whole
            -- function.
            let nit = evertStream $ \s1 -> do h <- liftIO $ openBinaryFile (fp `subst` lbl br) WriteMode
                                              liftIO $ modifyIORef hs $ (:) h
                                              writeBamHandle h hdr s1
            it <- afford (M.lookupDefault nit (lbl br) m) br
            go hs (M.insert (lbl br) it m) s

    subst [            ] _ = []
    subst ('%':'s':rest) l = unpack l ++ subst rest l
    subst ('%':'%':rest) l = '%' : subst rest l
    subst ( c :    rest) l =  c  : subst rest l


data MultiMapper = MultiMapper Bytes BamKey Int deriving (Typeable, Show)

instance Exception MultiMapper where
    displayException (MultiMapper qname bkey bval) =
        "Read " ++ unpack qname ++ " is a multimapper with " ++ shows bkey "==" ++ shows bval "."

check_multi_flags :: BamRec -> LIO ()
check_multi_flags b = check "NH" $ check "IH" $ check "HI" $ pure ()
  where
    check k m = case extAsInt 1 k b of
        1 -> m
        v -> logMsg Warning $ MultiMapper (b_qname b) k v

clean_multi_flags :: BamRec -> Maybe BamRec
clean_multi_flags b = if extAsInt 1 "HI" b /= 1 then Nothing else Just b'
  where
    b' = b { b_exts = deleteE "HI" $ deleteE "IH" $ deleteE "NH" $ b_exts b }

-- | Groups sorted records and selectively maps a function over some of
-- these groups, while preserving sorting.
--
-- This function is applied twice, once for the normalization phase and
-- once for the wrapping around phase.  Normalization only ever
-- increases coordinates, so ordering is preserved and we can stream
-- most of the reads;  this is done if the 'order' parameter is
-- 'Preserved'.
--
-- Wrapping sometimes decreases coordinates, so we need to buffer
-- everything to restore sorting.  We simply run everything through a
-- 'PriorityQueue' to get full sorting and controlled external
-- buffering.  While some microoptimizations are possible, this is easy,
-- robust, and tunable.

data Order = Destroyed | Preserved

mapSortAtGroups :: MonadIO m => PQ_Conf m -> Order -> IntMap a -> (a -> BamRec -> [BamRec])
                             -> Stream (Of BamRec) m r -> Stream (Of BamRec) m r
mapSortAtGroups cf order m f = no_group
  where
    no_group = lift . inspect >=> either pure no_group_1

    no_group_1 (a :> s) =
        case I.lookup (fromIntegral . unRefseq $ b_rname a) m of
            Nothing  -> wrap (a :> no_group s)
            Just arg -> lift (pushTo makePQ $ f arg a) >>= cont_group (b_rname a) arg s

    cont_group rn arg s0 pq = lift (inspect s0) >>= either flush_eof check1
      where
        flush_eof r = streamPQ maxBound pq >> pure r
        flush_go  s = streamPQ maxBound pq >> no_group_1 s
        check1 (a :> s)
            | b_rname a == rn = do pq' <- lift $ pushTo pq $ f arg a
                                   case order of
                                       Preserved -> streamPQ (rn, b_pos a) pq' >>= cont_group rn arg s
                                       Destroyed -> cont_group rn arg s pq'
            | otherwise       = flush_go (a :> s)

    -- Sends a list of records into a PQ.
    pushTo = foldM (\q -> liftIO . packBam >=> \b -> enqueuePQ cf (BySelfPos b) q)

    -- Streams from the PQ anything with position less than 'pos'.
    streamPQ :: MonadIO m => (Refseq, Int) -> PQ BySelfPos -> Stream (Of BamRec) m (PQ BySelfPos)
    streamPQ pos pq =
        case viewMinPQ pq of
            Just (BySelfPos r, pq')
                | coordinates r <= pos -> wrap (unpackBam r :> (liftIO pq' >>= streamPQ pos))
            _                          -> pure pq


-- | Normalize a read's alignment to fall into the "forward canonical
-- region" of [e..e+l].  Takes the name of the reference sequence, its
-- length and its padded length.  This normalization is ensured to work
-- (doesn't need information from the mate) and always increases
-- coordinates.
normalizeFwd :: Bytes -> Int -> Int -> BamRec -> BamRec
normalizeFwd nm lnat lpad b = b { b_pos  = norm $ b_pos  b
                                , b_mpos = norm $ b_mpos b
                                , b_mapq = if dups_are_fine then Q 37 else b_mapq b
                                , b_exts = if dups_are_fine then deleteE "XA" (b_exts b) else b_exts b }
  where
    dups_are_fine  = all_match_XA (extAsString "XA" b)
    all_match_XA s = case S.split ';' s of [xa1, xa2] | S.null xa2 -> one_match_XA xa1
                                           [xa1]                   -> one_match_XA xa1
                                           _                       -> False
    one_match_XA s = case S.split ',' s of (sq:pos:_) | sq == nm   -> pos_match_XA pos ; _ -> False
    pos_match_XA s = case S.readInt s   of Just (p,z) | S.null z   -> int_match_XA p ;   _ -> False
    int_match_XA p | p >= 0    = norm  (p-1) == norm (b_pos b) && not (isReversed b)
                   | otherwise = norm (-p-1) == norm (b_pos b) && isReversed b

    norm p = o + (p-o) `mod` lnat where o = lpad - lnat


newtype BySelfPos = BySelfPos BamRaw

instance Eq BySelfPos where
    BySelfPos a == BySelfPos b =
        coordinates a == coordinates b

instance Ord BySelfPos where
    BySelfPos a `compare` BySelfPos b =
        coordinates a `compare` coordinates b

instance Extern BySelfPos where
    usedBytes (BySelfPos   r) = S.length (raw_data r) +  8 * sizeOf (0::Int)
    extern    (BySelfPos   r) = put_byte_string (raw_data r)
    intern                    = liftM BySelfPos . bamRaw 0 =<< get_byte_string

