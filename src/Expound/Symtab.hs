module Expound.Symtab where

import Bio.Prelude
import Control.Monad.Trans.State.Strict

import qualified Data.ByteString        as S
import qualified Data.HashMap.Strict    as M
import qualified Data.IntervalMap       as I

type Gene       = Bytes
type Chrom      = Bytes
type ChromTable = M.HashMap Bytes (Pair Chrom Int)

-- | The interval is half-open, naturally.
data Region = Region
    { region_name   :: !Bytes
    , region_chrom  :: !Bytes
    , region_senses :: !Senses
    , region_start  :: !Int
    , region_end    :: !Int }
  deriving Show

eraseStrand :: Region -> Region
eraseStrand (Region n c _ s e) = Region n c Both s e

data Senses = Neither | Forward | Reverse | Both deriving ( Show, Enum )

withSenses :: Senses -> ((a -> Either a a) -> b) -> [b]
withSenses Neither _ = []
withSenses Forward k = [k Right]
withSenses Reverse k = [k Left]
withSenses Both    k = [k Left, k Right]

-- We use `Right` for forward, and `Left` for reverse,
-- just like when driving on roads.
type Annotab    = M.HashMap (Either Chrom Chrom) (I.IntervalMap Gene)

-- only needed so we store each string exactly once
type Symtab     = M.HashMap Gene Gene

type AnnoM = StateT (Pair Symtab Annotab)

intern :: Monad m => Bytes -> AnnoM m Bytes
intern sym = state $ \(s :!: t) ->
    case M.lookup sym s of
        Just x  -> (x, s :!: t)
        Nothing -> let !sym' = S.copy sym
                   in (sym', M.insert sym' sym' s :!: t)

annoChrom :: Monad m => Either Chrom Chrom -> Int -> Int -> Gene -> AnnoM m ()
annoChrom k a b g = modify' $ \(s :!: anno) -> s :!: M.alter (Just . I.insert (I.i2w $ min a b) (I.i2w $ max a b) g . fromMaybe I.empty) k anno

getSyms :: Monad m => AnnoM m Symtab
getSyms = (\(a :!: _) -> a) <$> get

getAnno :: Monad m => AnnoM m Annotab
getAnno = (\(_ :!: b) -> b) <$> get

execAnnoM :: Monad m => Symtab -> Annotab -> AnnoM m a -> m (Pair Symtab Annotab)
execAnnoM s t m = execStateT m (s :!: t)

