{-# LANGUAGE ExistentialQuantification #-}
module Expound.FormattedIO
    ( AnnoSet(..)
    , Annotation

    , Output(..)
    , outputPattern
    , outputSummary
    , outputTable

    , readChromTable
    , readGeneTable
    , readInput
    , readMyBed
    , read5Col

    , BedFormatError(..)
    , FastaError(..)
    , FcolFormatError(..)
    , SamFormatError(..)
    , UnknownChromosome(..)
    , UnknownInput(..)
    ) where

import Bio.Bam                   hiding ( Region(..) )
import Bio.Prelude
import Expound.Symtab
import System.FilePath                  ( splitExtension, splitFileName )
import System.IO

import qualified Bio.Streaming.Bytes                as S
import qualified Bio.Streaming.Prelude              as Q
import qualified Data.ByteString                    as B
import qualified Data.ByteString.Char8              as C
import qualified Data.HashMap.Strict                as M

data AnnoSet = Hits [Bytes] | NearMiss [Bytes] !Int deriving Show
type Annotation = ( Bytes, AnnoSet )

-- | Composite output function.  An effect to run once, an effectful
-- function that consumes the annotations for one file and returns some
-- sort of summary, and an effectful function that processes the
-- resulting list of summaries.
data Output = forall u . Output
    (LIO ())                                                              -- runs at the beginning
    (forall r . FilePath -> Stream (Of Annotation) LIO r -> LIO (Of u r)) -- runs once per input file
    ([u] -> LIO ())                                                       -- runs at the end

outputTable :: FilePath -> Output
outputTable "-" = Output (print_header stdout) (const $ make_table stdout) (const $ return ())
outputTable  f  = Output           (return ())              b              (const $ return ())
  where
    b _ s = bracket (liftIO $ openBinaryFile f WriteMode) (liftIO . hClose)
                    (\hdl -> print_header hdl >> make_table hdl s)

print_header :: MonadIO m => Handle -> m ()
print_header hdl = liftIO $ hPutStr hdl "#RegionName\tID\n"

make_table :: MonadIO m => Handle -> Stream (Of Annotation) m r -> m (Of () r)
make_table hdl = fmap (() :>) . Q.mapM_ (liftIO . format)
  where
    format (_, Hits [     ]) = return ()
    format (n, Hits (h1:hs)) = do C.hPut hdl n ; hPutChar hdl '\t'
                                  C.hPut hdl h1
                                  forM_ hs $ \h -> hPutChar hdl ':' >> C.hPut hdl h
                                  hPutChar hdl '\n'
    format (_, NearMiss [     ] _) = return ()
    format (n, NearMiss (a1:as) d) = do C.hPut hdl n ; hPutChar hdl '\t'
                                        C.hPut hdl a1 ; hPutChar hdl '@'
                                        forM_ as $ \a -> hPutChar hdl ':' >> C.hPut hdl a
                                        hPutStr hdl (show d)
                                        hPutChar hdl '\n'

outputSummary :: Output
outputSummary = Output (return ()) make_summary (liftIO . print_summary)
  where
    make_summary :: Monad m => a -> Stream (Of (t, AnnoSet)) m r -> m (Of (a, HashMap Bytes Int) r)
    make_summary f = Q.fold count_annos M.empty ((,) f)
      where
        count_annos m (_, Hits hs) = foldl' count1 m hs
        count_annos m _ = m
        count1 m h = (M.insert h $! 1 + M.lookupDefault (0::Int) h m) m

    print_summary :: [(FilePath, HashMap Bytes Int)] -> IO ()
    print_summary mm = do
        let keymap = M.unions $ map (M.map (const ()) . snd) mm
        putStr "ID"
        forM_ mm $ \(f,_) -> do putChar '\t' ; putStr f
        putChar '\n'
        sequence_ [ do C.putStr anno
                       forM_ mm $ \(_,counts) -> do let v = M.lookupDefault 0 anno counts
                                                    putChar '\t' ; putStr (show v)
                       putChar '\n'
                  | ( anno, () ) <- M.toList keymap ]


outputPattern :: FilePath -> Output
outputPattern pat = Output (return ()) write_file_pattern (const $ return ())
  where
    write_file_pattern :: (MonadIO m, MonadMask m) => FilePath -> Stream (Of Annotation) m r -> m (Of () r)
    write_file_pattern fname str =
        bracket (liftIO $ openFile (make_oname pat) WriteMode) (liftIO . hClose)
                (\hdl -> print_header hdl >> make_table hdl str)
      where
        (qname, ext) = splitExtension fname
        (path, name) = splitFileName qname

        make_oname ('%':'%':cs) = '%' : make_oname cs
        make_oname ('%':'s':cs) = fname ++ make_oname cs
        make_oname ('%':'f':cs) = qname ++ make_oname cs
        make_oname ('%':'p':cs) = path ++ make_oname cs
        make_oname ('%':'n':cs) = name ++ make_oname cs
        make_oname ('%':'e':cs) = ext ++ make_oname cs
        make_oname (      c:cs) = c : make_oname cs
        make_oname [          ] = [ ]


readGeneTable :: Monad m => Stream (Of Region) (AnnoM m) () -> AnnoM m ()
readGeneTable stream = do
    flip Q.mapM_ stream $ \(Region gi crm strands s e) -> do
        sym <- intern gi
        sequence_ $ withSenses strands $ \str ->
            annoChrom (str crm) s e sym

readChromTable :: Monad m => ByteStream m () -> m ChromTable
readChromTable = Q.fold_ add M.empty id . Q.map C.words . S.lines'
  where
    add m (x:_)            | "x" `C.isPrefixOf` x         = m
    add m (ens:ucsc:ofs:_) | Just (y,"") <- C.readInt ofs = M.insert (C.copy ens) (C.copy ucsc :!: y) m
    add m _                                               = m

readMyBam :: Monad m => BamMeta -> Stream (Of BamRaw) m r -> Stream (Of Region) m r
readMyBam hdr = Q.map xlate . Q.filter (not . isUnmapped) . Q.map unpackBam
  where
    getref r = sq_name $ getRef (meta_refs hdr) r
    str    r = if b_flag r .&. 0x10 == 0 then Forward else Reverse
    xlate  r = Region (b_qname r) (getref $ b_rname r) (str r) (b_pos r) (b_pos r + alignedLength (b_cigar r))

data SamFormatError = SamFormatError Bytes deriving (Typeable, Show)
instance Exception SamFormatError where
    displayException (SamFormatError s) = "Parse error in SAM line " ++ show s

-- | Very simple SAM parser.  can't use ordinary sam parser, because
-- translation to reference index and back won't work.
readMySam :: MonadLog m => ByteStream m r -> Stream (Of Region) m r
readMySam = Q.concat . Q.mapM parse1 . S.lines'
  where
    parse1 s = case p'sam $ B.split 9 s of
        Nothing -> Nothing <$ logMsg Error (SamFormatError s)
        Just  r -> pure $ Just r

    p'sam :: [Bytes] -> Maybe Region
    p'sam (qname : flag_ : rname : pos_ : cigar : _) = do
        (flag,"") <- C.readInt flag_
        (pos, "") <- C.readInt pos_
        len       <- readCigar 0 cigar

        let str = if flag .&. 0x10 == 0 then Forward else Reverse
        guard $ flag .&. 0x4 == 0
        return $! Region qname rname str (pos-1) (pos-1+len)
    p'sam _ = Nothing

    readCigar :: Int -> Bytes -> Maybe Int
    readCigar !l !w
        | C.null  w = Just l
        | otherwise = do (i,w1) <- C.readInt w
                         (c,w2) <- C.uncons w1
                         let l' = if c == 'M' || c == 'D' || c == 'N' then l+i else l
                         readCigar l' w2


data UnknownInput = UnknownInput deriving (Typeable, Show)
instance Exception UnknownInput where
    displayException _ = "input format not recognizable as BAM, SAM, FastA, or BED"

-- 'MonadIO' is needed to support gunzip.
readInput :: (MonadIO m, MonadLog m) => ByteStream m r -> Stream (Of Region) m r
readInput = lift . S.splitAt' 4 . S.gunzip >=> try_bam
  where
    try_bam ("BAM\SOH" :> s) = lift (decodePlainBam s) >>= uncurry readMyBam
    try_bam (magic     :> s) = lift (S.uncons (S.consChunk magic s)) >>= try_text

    try_text (Left r)  = pure r                                                         -- empty?!
    try_text (Right (c,s))
        | c == c2w '>' = readMyFasta (S.cons c s)                                       -- Fasta
        | c == c2w '@' = lift (decodePlainSam $ S.cons c s) >>= uncurry readMyBam       -- probably SAM
        | otherwise    = lift (S.toStrict (S.break (not . isPrinting_w8) $ S.cons c s) >>= mapM S.uncons) >>= try_sam

    try_sam (l1 :> Left r)   = try_sam1 l1 (pure r)                                     -- single-line SAM?!
    try_sam (l1 :> Right (c1, s))
        | c1 /= 10 && c1 /= 13 = lift (logMsg Error UnknownInput >> S.effects s)        -- binary junk
        | otherwise            = try_sam1 l1 (S.cons c1 s)                              -- headerless SAM?

    try_sam1 l1 s = lift (getSamRec (const invalidRefseq) l1) >>=
                    either (const $ readMyBed (S.consChunk l1 s))                       -- whatever, probably BED
                           (const $ readMySam (S.consChunk l1 s))                       -- headless SAM

    isPrinting_w8 c = (c >= 32 && c < 127) || c == 9


data FastaError = FastaError Bytes deriving (Typeable, Show)
instance Exception FastaError where
    displayException l = "parse error in Fasta file at " ++ show l

readMyFasta :: MonadLog m => ByteStream m r -> Stream (Of Region) m r
readMyFasta = Q.concat . Q.concat . Q.mapM proc_line . Q.filter (C.isPrefixOf ">") . S.lines'
  where
    proc_line = mapM parse_label . C.split ';' . C.drop 1
    parse_label label =
        maybe (Nothing <$ logMsg Error (FastaError label)) (pure . Just) $ do
            let (crm, l1) = C.break (':'==) label
            let (str, l2) = C.break (':'==) $ C.drop 1 l1
            (lft, l3) <- C.readInt $ C.drop 1 l2
            (rht,  _) <- C.readInt $ C.drop 1 l3
            return $! Region label crm (read_strand [str]) lft rht

data BedFormatError = BedFormatError Bytes deriving (Typeable, Show)
instance Exception BedFormatError where
    displayException (BedFormatError s) = "Parse error in BED line " ++ show s

readMyBed :: MonadLog m => ByteStream m r -> Stream (Of Region) m r
readMyBed = Q.foldrT parse1
          . Q.filter (not . C.isPrefixOf "track")
          . Q.filter (not . C.isPrefixOf "#")
          . S.lines'
  where
    parse1 s k = maybe (lift (logMsg Error (BedFormatError s))) Q.yield (p'bed s) >> k

    p'bed :: Bytes -> Maybe Region
    p'bed bs = do (crm : s_ : e_ : gi : strand) <- pure $ B.split 9 bs
                  (s,"") <- C.readInt s_
                  (e,"") <- C.readInt e_
                  pure $! Region gi crm (read_strand $ drop 1 strand) s e

read_strand :: [Bytes] -> Senses
read_strand [   ]                   = Both
read_strand (s:_) | C.null s        = Both
                  | C.head s == '-' = Reverse
                  | s == "0"        = Both
                  | otherwise       = Forward

data FcolFormatError = FcolFormatError [Bytes] deriving (Typeable, Show)
instance Exception FcolFormatError where
    displayException (FcolFormatError ws) = "error in legacy annotation file: " ++ show (C.unwords ws)

data UnknownChromosome = UnknownChromosome Bytes deriving (Typeable, Show)
instance Exception UnknownChromosome where
    displayException (UnknownChromosome nm) =
        "chromosome name and offset unknown: " ++ unpack nm

-- | Backwards-compatible crazyness.  This reads an underspecified
-- five-column format.  Basically bed, but jumbled.  In addition,
-- chromosome names can be translated, optionally with an offset.  If
-- chromosome translation is active, intervals are assumed to be 1-based
-- and closed.  This magic may result in confusing behavior.

read5Col :: MonadLog m => Maybe ChromTable -> ByteStream m r -> Stream (Of Region) m r
read5Col mct0 = fmap Q.snd' . Q.foldM go (pure mct0) pure . hoist lift . Q.map C.words . S.lines'
  where
    go :: MonadLog m => Maybe ChromTable -> [Bytes] -> Stream (Of Region) m (Maybe ChromTable)
    go mct [   ]                      = pure mct
    go mct (w:_) | C.head w == '#'    = pure mct
    go mct (gi:crm:s:e:strand)
        | Just (s',"") <- C.readInt s
        , Just (e',"") <- C.readInt e = xlate mct $ Region gi crm (read_strand strand) s' e'
    go mct ws                         = mct <$ lift (logMsg Error $ FcolFormatError ws)

    xlate :: MonadLog m => Maybe ChromTable -> Region -> Stream (Of Region) m (Maybe ChromTable)
    xlate  Nothing            rgn         = Nothing <$ Q.yield rgn
    xlate (Just ct) (Region n crm ss s e) =
        case M.lookup crm ct of
            Just (c :!: o) -> Just ct <$ Q.yield (Region n c ss (s+o-1) (e+o))
            Nothing        -> do
                -- warn, don't translate, but remember the chromosome so we don't warn again
                lift $ logMsg Warning $ UnknownChromosome crm
                Just (M.insert crm (crm :!: 0) ct) <$ Q.yield (Region n crm ss (s-1) e)


