module Expound.Network
    ( Client
    , Request(..)
    , Response(..)
    , Which(..)
    , getRequest
    , getResponse
    , putRequest
    , putResponse
    , receive
    , send
    , service
    ) where

import Bio.Prelude
import Bio.Streaming.Parse                   ( EofException(..) )
import Control.Monad.Trans.Reader            ( ReaderT, ask )
import Control.Monad.Trans.State.Strict      ( StateT, get, put )
import Network.Socket                        ( Socket, close )
import Network.Socket.ByteString             ( recv )
import Network.Socket.ByteString.Lazy        ( sendAll )

import qualified Data.Binary.Get                    as B
import qualified Data.ByteString                    as B
import qualified Data.ByteString.Builder            as B

import Expound.FormattedIO
import Expound.Symtab

data Which  =  Covered | Covering | Fragment | Inclusive | Nearest deriving (Enum, Show)

--  The protocol, initially:
--  -> StartAnno name
--          Open the annotation set named "name".
--  <- KnownAnno
--          The server already has this set and brought it into scope or
--  <- UnknownAnno
--          The server doesn't know about this set and created it.
--
--  With an open annotation set:
--  -> AddAnno region
--          Add to the currently open annotation set or
--  -> EndAnno
--          The set is complete now and to be brought into scope.
--
--  At any time:
--  -> Anno which region
--          Request annotation of "region" using lookup strategy
--          "which" from all sets in scope.
--  <- Result name annos
--          Resulting annotation for the region named "name".

data Request = StartAnno Bytes
             | AddAnno Region
             | EndAnno
             | StartFile Bytes
             | Anno Which Region
             | EndStream
    deriving Show

data Response = UnknownAnno
              | KnownAnno
              | StartedFile Bytes
              | Result Bytes AnnoSet
              | EndedStream
    deriving Show

putWord :: ( Bits a, Integral a ) => a -> B.Builder
putWord i | i < 0x80  =    B.word8 (fromIntegral i)
          | otherwise =    B.word8 ((fromIntegral i .&. 0x7f) .|. 0x80)
                        <> putWord (i `shiftR` 7)

getWord :: ( Bits a, Integral a ) => B.Get a
getWord = do
    w <- B.getWord8
    if w < 0x80 then return (fromIntegral w)
                else do w' <- getWord
                        return $ (w' `shiftL` 7) .|. (fromIntegral w .&. 0x7f)

getRequest :: B.Get Request
getRequest = do key <- getWord
                case key of
                    0 -> StartAnno <$> getString
                    1 -> AddAnno <$> getRegion
                    2 -> return EndAnno
                    3 -> StartFile <$> getString
                    4 -> return EndStream
                    _ -> Anno (toEnum (key - 5)) <$> getRegion

getRegion :: B.Get Region
getRegion = Region <$> getString
                   <*> getString
                   <*> fmap toEnum getWord
                   <*> getWord
                   <*> getWord

putRegion :: Region -> B.Builder
putRegion (Region n m s u v) =    putString n
                               <> putString m
                               <> putWord (fromEnum s)
                               <> putWord u
                               <> putWord v

putRequest :: Request -> B.Builder
putRequest (StartAnno name) =    putWord (0::Int)
                              <> putString name
putRequest (AddAnno region) =    putWord (1::Int)
                              <> putRegion region
putRequest  EndAnno         =    putWord (2::Int)
putRequest (StartFile name) =    putWord (3::Int)
                              <> putString name
putRequest  EndStream       =    putWord (4::Int)
putRequest (Anno w region)  =    putWord (fromEnum w + 5)
                              <> putRegion region

getResponse :: B.Get Response
getResponse = do key <- getWord
                 case key :: Int of
                    0 -> return UnknownAnno
                    1 -> return KnownAnno
                    3 -> StartedFile <$> getString
                    4 -> return EndedStream
                    _ -> do n <- getString
                            l <- getWord
                            case mod l 4 of 0 -> do as <- getStrings id (div l 4)
                                                    d  <- getWord
                                                    return (Result n (NearMiss as d))
                                            1 -> do as <- getStrings id (div l 4)
                                                    d  <- getWord
                                                    return (Result n (NearMiss as (-d)))
                                            _ -> do hs <- getStrings id (div l 4)
                                                    return (Result n (Hits hs))
  where
    getStrings acc n | n <= (0::Int) = pure $ acc []
                     | otherwise     = getString >>= \s -> getStrings (acc . (:) s) (n-1)

putResponse :: Response -> B.Builder
putResponse  UnknownAnno  = putWord (0::Int)
putResponse  KnownAnno    = putWord (1::Int)
putResponse (Result n rs) =    putWord (2::Int)
                            <> putString n
                            <> case rs of NearMiss as d ->    putWord (length as * 4 + if d >= 0 then 0 else 1)
                                                           <> foldMap putString as
                                                           <> putWord (abs d)
                                          Hits hs       ->    putWord (length hs * 4 + 2)
                                                           <> foldMap putString hs
putResponse (StartedFile nm) = putWord (3::Int) <> putString nm
putResponse  EndedStream     = putWord (4::Int)


getString :: B.Get Bytes
getString = getWord >>= B.getByteString

putString :: Bytes -> B.Builder
putString s = putWord (B.length s) <> B.byteString s

data ParseError = ParseError String deriving (Show,Typeable)
instance Exception ParseError where
    displayException (ParseError m)
        = "Error parsing network packet: " ++ m

service :: MonadIO m => B.Get a -> (b -> B.Builder) -> (s -> a -> m (s, Maybe b)) -> Socket -> s -> m s
service g p f sk = go (B.runGetIncremental g)
  where
    go (B.Done rest _ a) s = do (s',b) <- f s a
                                liftIO $ forM_ b (sendAll sk . B.toLazyByteString . p)
                                go (B.runGetIncremental g `B.pushChunk` rest) s'
    go (B.Fail    _ _ m) _ = do liftIO $ close sk
                                liftIO $ throwIO $ ParseError m
    go (B.Partial     k) s = do inp <- liftIO $ recv sk 4000
                                if B.null inp
                                    then s <$ liftIO (close sk)
                                    else go (k $ Just inp) s


type Client m = ReaderT Socket (StateT Bytes m)

receive :: MonadIO m => B.Get a -> Client m a
receive getter = do
    sock <- ask
    incoming <- lift get
    go sock (B.runGetIncremental getter `B.pushChunk` incoming)
  where
    go _sock (B.Fail    _ _ m) = liftIO $ throwIO $ ParseError m
    go _sock (B.Done rest _ a) = lift (put rest) >> return a
    go  sock (B.Partial     k) = do inp <- liftIO $ recv sock 4000
                                    if B.null inp then liftIO (close sock) >> liftIO (throwIO EofException)
                                                  else go sock $ k $ Just inp

send :: MonadIO m => B.Builder -> Client m ()
send putter = do sock <- ask ; liftIO $ sendAll sock $ B.toLazyByteString putter
