{-# LANGUAGE Rank2Types, LambdaCase, TemplateHaskell #-}
{-
This is a validator/fixup for paired end BAM files, that is more
efficient than 'samtools sort -n' followed by 'samtools fixmate'.

We want both: to quickly join separate mates together again from the
information about the mate's mapping coordinate, but at the same time
deal with broken files where that doesn't actually work.  Whenever we
join mates, we also check if the flags are consistent and fix them if
they aren't.

We look ahead one read in case pairs are already grouped.  Else we use
the mate position if present, and finally fall back to sorting by name
whenever that fails.  This approach works...

 - splendidly, if mates are already adjacent, in which case everything
   is streamed.
 - well, if the input is sorted properly, in which case most reads
   stream, but improper pairs need to queue until the mate is reached.
 - reasonably, if there are occasional widows, which will be queued
   to the very end and sorted by hashed-qname before they are recognized
   and repaired.
 - awkwardly, if sorting is violated, flags are wrong or widows are
   the rule, because then it degenerates to a full sort by qname.
-}

import Bio.Bam
import Bio.Prelude                       hiding ( yield )
import Bio.Util.Git
import Data.PriorityQueue.External
import Options.Applicative
import Paths_biohazard_tools                    ( version )

import qualified Bio.Streaming.Prelude    as Q
import qualified Data.ByteString          as S
import qualified Data.IntSet              as I
import qualified Data.Vector.Generic      as V

data Verbosity = Silent   | Errors | Warnings | Notices deriving (Eq, Ord)
data KillMode  = KillNone | KillUu | KillAll            deriving (Eq, Ord)
type OFn m     = BamMeta -> Stream (Of FixedBams) m () -> m ExitCode
data Infilter  = ConstTrue | MappedOnly

data Config m = CF { output       :: OFn m
                   , killmode     :: KillMode
                   , report_mrnm  :: Bool
                   , report_mpos  :: Bool
                   , report_isize :: Bool
                   , report_flags :: Bool
                   , report_fflag :: Bool
                   , report_ixs   :: Bool
                   , infilter     :: Infilter
                   , pqconf       :: PQ_Conf m
                   , files        :: [FilePath] }

-- Output options.  Includes 'X' so that it generates documentation, but
-- it isn't actually processed.
ooptions :: Parser (OFn LIO)
ooptions =
    ((\f h s -> ExitSuccess <$ writeBamFile f h s) <$> strOption
        (short 'o' <> long "output" <> metavar "FILE" <> help "Write output to FILE") <|>
     flag' (\_ s -> ExitSuccess <$ Q.effects s)
        (short 'n' <> long "dry-run" <> long "validate" <> help "No output, validate only") <|>
     option disabled (short 'X' <> long "exec" <> metavar "COMMAND" <> help "Send FastQ output to a program") <|>
     pure (\h s -> ExitSuccess <$ pipeBamOutput h (protectTerm s)))

options :: Parser (OFn LIO) -> Parser (Config LIO)
options oo = CF
    <$> oo
    <*> (flag' KillAll (short 'k' <> long "kill-widows" <> help "Delete all widows") <|>
         flag' KillUu  (short 'u' <> long "kill-unmapped" <> help "Delete unmapped widows") <|>
         flag' KillNone             (long "kill-none" <> help "Never delete widows (default)") <|>
         pure  KillNone)

    <*> (flag' True  (long "report-mrnm"     <> help "Report wrong mate reference name (default yes)") <|>
         flag' False (long "no-report-mrnm"  <> help "Do not report wrong mate reference name") <|>
         pure  True)
    <*> (flag' True  (long "report-mpos"     <> help "Report wrong mate position (default yes)") <|>
         flag' False (long "no-report-mpos"  <> help "Do not report wrong mate position") <|>
         pure  True)
    <*> (flag' True  (long "report-isize"    <> help "Report wrong insert size (default no)") <|>
         flag' False (long "no-report-isize" <> help "Do not report wrong insert size") <|>
         pure  False)
    <*> (flag' True  (long "report-flags"    <> help "Report wrong flags (default yes)") <|>
         flag' False (long "no-report-flags" <> help "Do not report wrong flags") <|>
         pure  True)
    <*> (flag' True  (long "report-fflag"    <> help "Report commonly inconsistent flags (default no)") <|>
         flag' False (long "no-report-fflag" <> help "Do not report commonly inconsistent flags") <|>
         pure  False)
    <*> (flag' True  (long "report-ixs"      <> help "Report mismatched index fields (default yes)") <|>
         flag' False (long "no-report-ixs"   <> help "Do not report mismatched index fields") <|>
         pure  True)

    <*> flag ConstTrue MappedOnly (long "only-mapped" <> help "Ignore totally unmapped input")
    <*> (PQ_Conf
        <$> option auto (short 'M' <> long "max-memory" <> value 1000 <>
                         metavar "MB" <> help "Use at most MB megabytes per queue")
        <*> option auto (short 'L' <> long "max-merge" <> value 200 <>
                         metavar "NUM" <> help "Merge at most NUM files at a time")
        <*> option auto (short 'T' <> long "temp-path" <> value "/var/tmp/" <>
                         metavar "PATH" <> help "Store temporary files in PATH")
        <*> pure logStringLn)
    <*> many (strArgument (metavar "FILE"))


mapped_only :: BamPair -> Bool
mapped_only p = case p of
        Singleton a -> okay a
        LoneMate  a -> okay a
        Other     a -> okay a
        Pair    a b -> okay a || okay b
  where
    okay = (\r -> not (isUnmapped r) || (isPaired r && not (isMateUnmapped r))) . unpackBam


pipe_to :: FilePath -> [String] -> OFn LIO
pipe_to cmd args _ =
    pipeToCmd cmd args . Q.foldrT conv
  where
    conv (a :!: b) = conv1 a . conv1 b

    conv1  Dropped       = id
    conv1 (Unchanged br) = \s -> liftIO (formatFastq (unpackBam br)) >>= flip Q.cons s
    conv1 (Fixed  br fu) = \s -> liftIO (formatFastq (fixup fu (unpackBam br))) >>= flip Q.cons s


main :: IO ()
main = do (args,cmd) <- break (`elem` ["-X","--exec"]) `fmap` getArgs
          exitWith =<< case cmd of
              _:cmd':args' -> withArgs args $ main' (pure (pipe_to cmd' args'))
              _            -> main' ooptions

main' :: Parser (OFn LIO) -> IO ExitCode
main' oopt =
   execWithParser_ (options oopt) (Just version) (giFullVersion $$gitInfo)
                   (header "Reunite paired end reads in BAM files" <> fullDesc <>
                    progDesc "Merge BAM files, rearrange them to move mate pairs together, \
                             \output a file with consistent mate pair information.  Alternatively, \
                             \pipe multiple FastQ streams in lock step to a program.")
                   $ \config -> do
      add_pg <- addPG $ Just version
      mergeInputs (files config) $ \hdr ->
            output config (add_pg hdr)
            . re_pair config (meta_refs hdr)
            . case infilter config of ConstTrue -> id ; MappedOnly -> Q.filter mapped_only

newtype FlagMismatch = FlagMismatch Bytes deriving (Typeable, Show)
instance Exception FlagMismatch where
    displayException (FlagMismatch nm) =
        "Names match, but 1st mate / 2nd mate flags do not: " ++ unpack nm


fixmate :: BamRaw -> BamRaw -> Mating r LIO FixedBams
fixmate r s | isFirstMate (unpackBam r) && isSecondMate (unpackBam s) = (:!:) <$> go r s <*> go s r
            | isSecondMate (unpackBam r) && isFirstMate (unpackBam s) = (:!:) <$> go s r <*> go r s
            | otherwise = do logMsg Error . FlagMismatch $! b_qname (unpackBam r) ; pure (Dropped :!: Dropped)
  where
    -- position of 5' end
    pos5 a = if isReversed a then b_pos a + alignedLength (b_cigar a) else b_pos a

    -- transfer info from b to a
    go p q = do infos <- filter (not . null) `fmap` sequence [ m | (_,m) <- problems ]
                unless (null infos) $ logStringLn $ message infos
                pure $! Fixed p $ Fixup (b_rname b) (b_mpos b) computedIsize computedFlag replaced_fields new_fields
      where
        a = unpackBam p
        b = unpackBam q

        problems = filter (\(x,_) -> not x) checks
        checks = [ (b_mrnm a  == b_rname b,     count_mrnm)
                 , (b_mpos a  == b_pos b,       count_mpos)
                 , (b_isize a == computedIsize, count_isize)
                 , (b_flag a === computedFlag,  count_flags)
                 , (b_flag a =!= computedFlag,  count_fflag)
                 , (b_indices a == common_indices, count_ixs) ]

        message infos = "fixing " ++ shows (b_qname a `S.append` if isFirstMate a then "/1" else "/2")
                        ": \t" ++ intercalate ", " infos
        !computedFlag' = (if b_rname a == invalidRefseq then (.|. flagUnmapped) else id) .
                         (if b_rname b == invalidRefseq then (.|. flagMateUnmapped) else id) .
                         (if isReversed b then (.|. flagMateReversed) else (.&. complement flagMateReversed)) .
                         (if isUnmapped b then (.|. flagMateUnmapped) else (.&. complement flagMateUnmapped)) .
                         (if isFailsQC  b then (.|. flagFailsQC) else id) $
                         b_flag a

        !properly_paired = computedFlag' .&. (flagUnmapped .|. flagMateUnmapped) == 0 && b_rname a == b_rname b
        !computedFlag    = if properly_paired then computedFlag' else computedFlag' .&. complement flagProperlyPaired
        !computedIsize   = if properly_paired then pos5 b - pos5 a else 0

        reduce f | f .&. flagMateUnmapped == 0 = f .&. complement flagFailsQC
                 | otherwise = f .&. complement (flagFailsQC .|. flagMateReversed)

        f1 === f2 = reduce f1 == reduce f2
        f1 =!= f2 = f1 /= f2 && f1 === f2

        onlyIf f m = (\z -> if z then m else "") `fmap` tells f

        count_mrnm  = do modify $ \c -> c { num_mrnm = 1 + num_mrnm c }
                         let ra = unRefseq (b_mrnm a); rb = unRefseq (b_rname b)
                         onlyIf report_mrnm $ printf "MRNM %d is wrong (%d)" ra rb

        count_mpos  = do modify $ \c -> c { num_mpos = 1 + num_mpos c }
                         onlyIf report_mpos $ printf "MPOS %d is wrong (%d)" (b_mpos a) (b_pos b)

        count_isize = do modify $ \c -> c { num_isize = 1 + num_isize c }
                         onlyIf report_isize $ printf "ISIZE %d is wrong (%d)" (b_isize a) computedIsize

        count_flags = do modify $ \c -> c { num_flags = 1 + num_flags c }
                         onlyIf report_flags $ printf "FLAG %03X is wrong (+%03X,-%03X)" (b_flag a) fp fm

        count_fflag = do modify $ \c -> c { num_fflag = 1 + num_fflag c }
                         onlyIf report_fflag $ printf "FLAG %03X is technically wrong (+%03X,-%03X)" (b_flag a) fp fm

        count_ixs = do modify $ \c -> c { num_ixs = 1 + num_ixs c }
                       onlyIf report_ixs $ printf "Index fields %s are wrong (%s)" (show $ b_indices a) (show common_indices)

        fp = computedFlag .&. complement (b_flag a)
        fm = complement computedFlag .&. b_flag a

        replaced_fields = I.fromList [ fromIntegral k | BamKey k <- index_fields ++ [ "MC", "ms" ] ]
        index_fields = [ "XI", "XJ", "YI", "YJ", "RG", "BC" ]

        b_indices x = map (`extAsString` x) index_fields
        common_indices = zipWith max (b_indices a) (b_indices b)

        new_fields = zip index_fields (map Text common_indices) ++
                     [ ( "MC", Text $ fromString $ V.foldr ((++) . show) [] $ b_cigar b )
                     , ( "ms", Int $ maybe 0 (V.sum . V.filter (>= 15) . V.map fromIntegral . V.map unQ) $ b_qual b ) ]


-- | Turns a widow into a single.  Basically removes the pairing
-- related flags and clear the information concerning the mate.
divorced :: BamRaw -> FixedBam
divorced b = Fixed b $ Fixup { f_flag    = b_flag (unpackBam b) .&. complement pair_flags
                             , f_mrnm    = invalidRefseq
                             , f_mpos    = invalidPos
                             , f_isize   = 0
                             , f_delexts = I.empty
                             , f_addexts = [] }
  where
    pair_flags = flagPaired .|. flagProperlyPaired .|.
                 flagFirstMate .|. flagSecondMate .|.
                 flagMateUnmapped .|. flagMateReversed


-- The mating algorithm works with priority queues alone:
--
-- - One contains incomplete pairs ordered by mate position.  When we
--   reach a given position and find the 2nd mate, the minimum in this
--   queue must be the 1st mate (or another 1st mate matching another
--   read we'll find here).
--
-- - One contains incomplete pairs ordered by (hash of) qname.  This one
--   is only used if we missed a mate for some reason.  After we read
--   the whole input, all remaining pairs can be pulled off this queue
--   in order of increasing (hash of) qname.
--
-- - At any given position, we will have a number of 1st mates that have
--   been waiting in the queue and a number of 2nd mates that are coming
--   in from the input.  We dump both sets into a queue by qname, then
--   pull them out in pairs.  Stuff that comes off as anything else than
--   a pair gets queued up again.

data MatingStats = MS { total_in   :: !Int
                      , total_out  :: !Int
                      , singletons :: !Int
                      , others     :: !Int
                      , widows     :: !Int
                      , num_mrnm   :: !Int
                      , num_mpos   :: !Int
                      , num_isize  :: !Int
                      , num_flags  :: !Int
                      , num_fflag  :: !Int
                      , num_ixs    :: !Int }

report_stats :: MatingStats -> [String]
report_stats ms = [
    "number of records read:          " ++ showNum (total_in ms),
    "number of records written:       " ++ showNum (total_out ms),
    "number of true singletons:       " ++ showNum (singletons ms),
    "number of other reads:           " ++ showNum (others ms),
    "number of widows:                " ++ showNum (widows ms),
    "number of repaired MRNM values:  " ++ showNum (num_mrnm ms),
    "number of repaired MPOS values:  " ++ showNum (num_mpos ms),
    "number of repaired ISIZE values: " ++ showNum (num_isize ms),
    "number of repaired FLAGS values: " ++ showNum (num_flags ms),
    "number of common FLAGS problems: " ++ showNum (num_fflag ms),
    "number of index field problems:  " ++ showNum (num_ixs ms) ]

data Queues = QS { right_here :: !(PQ ByQName)
                 , in_order   :: !(PQ ByMatePos)
                 , messed_up  :: !(PQ ByQName) }

type Lens s a = forall f. Functor f => (a -> f a) -> s -> f s

right_here' :: Lens Queues (PQ ByQName)
right_here' f q = (\p -> q { right_here = p }) <$> f (right_here q)
{-# INLINE right_here' #-}

in_order' :: Lens Queues (PQ ByMatePos)
in_order'   f q = (\p -> q { in_order   = p }) <$> f (in_order   q)
{-# INLINE in_order' #-}

messed_up' :: Lens Queues (PQ ByQName)
messed_up'  f q = (\p -> q { messed_up  = p }) <$> f (messed_up  q)
{-# INLINE messed_up' #-}

ms0 :: MatingStats
ms0 = MS 0 0 0 0 0 0 0 0 0 0 0

getSize :: (Queues -> PQ a) -> Mating r m Int
getSize sel = getq $ sizePQ . sel
{-# INLINE getSize #-}

enqueue :: (MonadIO m, Ord a, Extern a) => a -> Lens Queues (PQ a) -> Mating r m ()
enqueue a sel = Mating $ \k s q c r i -> lift (sel (enqueuePQ (pqconf c) a) q) >>= \q' -> k () s q' c r i
{-# INLINE enqueue #-}

peekMin :: (Ord a, Extern a) => (Queues -> PQ a) -> Mating r m (Maybe a)
peekMin sel = getq $ fmap fst . viewMinPQ . sel
{-# INLINE peekMin #-}

newtype W w m a = W { runW :: m (a,w) }
instance Monad m => Functor (W w m) where
    fmap f w = W (first f `liftM` runW w)
    {-# INLINE fmap #-}

fetchMin :: (MonadIO m, Ord a, Extern a) => Lens Queues (PQ a) -> Mating r m (Maybe a)
fetchMin sel =
    Mating $ \k s qs c r i ->
        lift (runW (sel f qs)) >>= \(qs', b) -> k b s qs' c r i
  where
    f q = W $ case viewMinPQ q of
            Nothing     ->                      return (q, Nothing)
            Just (a,mq) -> liftIO mq >>= \q' -> return (q', Just a)
{-# INLINE fetchMin #-}

discardMin :: (MonadIO m, Ord a, Extern a) => Lens Queues (PQ a) -> Mating r m ()
discardMin sel = fetchMin sel >> return ()
{-# INLINE discardMin #-}

report' :: MonadLog m => Mating r m ()
report' = do o <- gets total_out
             when (o `mod` 0x40000 == 0) $ do
                     ms <- getSize messed_up
                     logStringLn $ printf "out: %d, mess: %d" o ms
{-# INLINE report' #-}

report :: MonadLog m => BamRaw -> Mating r m ()
report br = do i <- gets total_in
               o <- gets total_out
               when (i `mod` 0x100000 == 0) $ do
                     hs <- getSize right_here
                     os <- getSize in_order
                     ms <- getSize messed_up
                     rr <- getRefseqs
                     let BamRec{..} = unpackBam br
                         rn = unpack . sq_name $ getRef rr b_rname
                         at = if b_rname == invalidRefseq || b_pos == invalidPos
                              then "" else printf "@%s/%d,\t" rn b_pos
                         off = fromIntegral $ b_virtual_offset `shiftR` 36
                     logString_ $ printf "%+5dMB, %sin: %9d, out: %9d, here: %6d, wait: %6d, mess: %6d"
                                         (off::Int) (at::String) i o hs os ms
{-# INLINE report #-}

data LostMate = LostMate !Bytes !BamRaw deriving Typeable

instance Show LostMate where
    show (LostMate l br) = "LostMate " ++ show l ++ " " ++ show (unpackBam br)

instance Exception LostMate where
    displayException (LostMate l br) =
        "[" ++ unpack l ++ "] record " ++
        shows (b_qname (unpackBam br)) (if isFirstMate (unpackBam br) then "/1" else "/2") ++
        " did not have a mate at the right location."

no_mate_here :: (MonadIO m, MonadLog m) => Bytes -> BamRaw -> Mating r m ()
no_mate_here l br = do let !br' = br_copy br
                       logMsg Notice $ LostMate l br'
                       enqueue (byQName br') messed_up'
{-# INLINE no_mate_here #-}

data NoMate = NoMate !Bytes !Int deriving (Typeable, Show)
instance Exception NoMate where
    displayException (NoMate nm ix) =
        "record " ++ shows nm " (" ++ shows ix ") did not have a mate at all."

no_mate_ever :: MonadLog m => BamRaw -> Mating r m ()
no_mate_ever b = do logMsg Notice $ NoMate (b_qname (unpackBam b)) (extAsInt 1 "XI" (unpackBam b))
                    modify $ \c -> c { widows = 1 + widows c }
                    kill <- tells killmode
                    case kill of
                        KillAll  -> return ()
                        KillUu   -> unless (isUnmapped (unpackBam b)) $ yield (divorced b :!: Dropped)
                        KillNone -> yield (divorced b :!: Dropped)
{-# INLINE no_mate_ever #-}

data FixedBam = Dropped
              | Unchanged !BamRaw
              | Fixed !BamRaw !Fixup

data Fixup = Fixup { f_mrnm  :: !Refseq
                   , f_mpos  :: !Int
                   , f_isize :: !Int
                   , f_flag  :: !Int
                   , f_delexts :: !IntSet
                   , f_addexts :: Extensions }

fixup :: Fixup -> BamRec -> BamRec
fixup fu b = b { b_mrnm = f_mrnm fu
               , b_mpos = f_mpos fu
               , b_isize = f_isize fu
               , b_flag = f_flag fu
               , b_exts = delexts (b_exts b) ++ f_addexts fu }
  where
    delexts = filter (\(BamKey k,_) -> not $ I.member (fromIntegral k) (f_delexts fu))
{-# INLINE fixup #-}

type FixedBams = Pair FixedBam FixedBam

instance IsBamRec FixedBam where
    unpackBamRec  Dropped      = [ ]
    unpackBamRec (Unchanged a) = unpackBamRec a
    unpackBamRec (Fixed  b fu) = [fixup fu $ unpackBam b]

    pushBam  Dropped      = id
    pushBam (Unchanged a) = pushBam a
    pushBam (Fixed  b fu) = pushBam $ fixup fu $ unpackBam b


newtype Mating r m a = Mating { runMating ::
    (a -> MatingStats -> Queues -> Config m -> Refs -> Stream (Of BamPair) m r -> Stream (Of FixedBams) m r)
       -> MatingStats -> Queues -> Config m -> Refs -> Stream (Of BamPair) m r -> Stream (Of FixedBams) m r }

instance Functor (Mating r m) where
    fmap f m = Mating $ \k -> runMating m (k . f)
    {-# INLINE fmap #-}

instance Applicative (Mating r m) where
    pure a = Mating $ \k -> k a
    {-# INLINE pure #-}
    u <*> v = Mating $ \k -> runMating u (\a -> runMating v (k . a))
    {-# INLINE (<*>) #-}
    u  *> v = Mating $ \k -> runMating u (\_ -> runMating v k)
    {-# INLINE (*>) #-}

instance Monad (Mating r m) where
    return = pure
    {-# INLINE return #-}
    m >>=  k = Mating $ \k2 -> runMating m (\a -> runMating (k a) k2)
    {-# INLINE (>>=) #-}
    (>>) = (*>)
    {-# INLINE (>>) #-}

instance MonadIO m => MonadIO (Mating r m) where
    liftIO f = Mating $ \k s q c r i -> liftIO f >>= \a -> k a s q c r i
    {-# INLINE liftIO #-}

instance MonadTrans (Mating r) where
    lift f = Mating $ \k s q c r i -> lift f >>= \a -> k a s q c r i
    {-# INLINE lift #-}

instance MonadLog m => MonadLog (Mating r m) where
    logMsg    l e = lift (logMsg    l e)
    {-# INLINE logMsg #-}
    logString_  e = lift (logString_  e)
    {-# INLINE logString_ #-}
    logStringLn e = lift (logStringLn e)
    {-# INLINE logStringLn #-}

tells :: (Config m -> a) -> Mating r m a
tells f = Mating $ \k s q c -> k (f c) s q c
{-# INLINE tells #-}

gets :: (MatingStats -> a) -> Mating r m a
gets f = Mating $ \k s -> k (f s) s
{-# INLINE gets #-}

getq :: (Queues -> a) -> Mating r m a
getq f = Mating $ \k s q -> k (f q) s q
{-# INLINE getq #-}

modify :: (MatingStats -> MatingStats) -> Mating r m ()
modify f = Mating $ \k s -> k () $! f s
{-# INLINE modify #-}

getRefseqs :: Mating r m Refs
getRefseqs = Mating $ \k s q c r -> k r s q c r
{-# INLINE getRefseqs #-}

fetchNext :: MonadLog m => Mating r m (Maybe BamPair)
fetchNext = Mating $ \k s q c r i ->
    lift (inspect i) >>= \case
        Left        z  -> k Nothing s q c r $ pure z
        Right (x :> j) -> case x of
            Singleton y -> kk y 1
            Other     y -> kk y 1
            Pair    _ y -> kk y 2
            LoneMate  y -> kk y 1
          where
            kk y n = let !s' = s { total_in = n + total_in s }
                     in runMating (report y) (const $ k (Just x)) s' q c r j
{-# INLINE fetchNext #-}

yield :: Monad m => FixedBams -> Mating r m ()
yield (a :!: b) = Mating $ \k s q c r i -> do
    let !s' = s { total_out = len a + len b + total_out s }
    Q.cons (a :!: b) $ k () s' q c r i
  where
    len Dropped = 0
    len _       = 1
{-# INLINE yield #-}

newtype OutOfOrder = OutOfOrder Bytes deriving (Typeable, Show)
instance Exception OutOfOrder where
    displayException (OutOfOrder nm) = "record " ++ unpack nm ++ " is out of order."

-- To ensure proper cleanup, we require the priority queues to be created
-- outside.  Since one is continually reused, it is important that a PQ
-- that is emptied no longer holds on to files on disk.
-- re_pair :: (MonadIO m, MonadLog m) => Config m -> Refs -> Q.Stream (Q.Of BamPair) m r -> Q.Stream (Q.Of [BamRec]) m r
re_pair :: Config LIO -> Refs -> Q.Stream (Q.Of BamPair) LIO r -> Q.Stream (Q.Of FixedBams) LIO r
re_pair = runMating go finish ms0 (QS makePQ makePQ makePQ)
   where
    go = fetchNext >>= go'

    -- At EOF, flush everything.
    go' Nothing = peekMin right_here >>= \case
            Just (ByQName _ _ qq) -> do complete_here (coordinates qq)
                                        flush_here Nothing  -- flush_here loops back here
            Nothing               -> flush_in_order  -- this ends the whole operation

    -- Single read?  Pass through and go on.
    -- Paired read?  Does it belong 'here'?
    go' (Just (Singleton x)) = modify (\c -> c { singletons = 1 + singletons c }) >> (yield $! Unchanged x :!: Dropped) >> go
    go' (Just (Other     x)) = modify (\c -> c { others     = 1 + others     c }) >> (yield $! Unchanged x :!: Dropped) >> go
    go' (Just (Pair    x y)) = fixmate x y >>= yield >> go
    go' (Just (LoneMate  r)) = peekMin right_here >>= \case

            -- there's nothing else here, so here becomes redefined
            Nothing             -> enqueueThis r >> go

            Just (ByQName _ _ qq) -> case compare (coordinates r) (coordinates qq) of
                -- nope, r is out of order and goes to 'messed_up'
                LT -> do logMsg Warning $ OutOfOrder (qnames r)
                         let !r' = br_copy r
                         enqueue (byQName r') messed_up'
                         go

                -- nope, r comes later.  we need to finish our business here
                GT -> do complete_here (coordinates qq)
                         flush_here (Just (LoneMate r))

                -- it belongs here or there is nothing else here
                EQ -> enqueueThis r >> go


    -- lonely guy, belongs either here or needs to wait for the mate
    enqueueThis r | coordinates r >= br_mate_pos r = enqueue (byQName r) right_here'
                  | otherwise             = r' `seq` enqueue (ByMatePos r') in_order'
        where r' = br_copy r

    -- Flush the in_order queue to messed_up, since those didn't find
    -- their mate the ordinary way.  Afterwards, flush the messed_up
    -- queue.
    flush_in_order = fetchMin in_order' >>= \case
        Just (ByMatePos b) -> no_mate_here "flush_in_order" b >> flush_in_order
        Nothing            -> flush_messed_up

    -- Flush the messed up queue.  Everything should come off in pairs,
    -- unless something is broken.
    flush_messed_up = fetchMin messed_up' >>= flush_mess1

    flush_mess1 Nothing                 = return ()
    flush_mess1 (Just (ByQName _ ai a)) = fetchMin messed_up' >>= flush_mess2 ai a

    flush_mess2  _ a Nothing = no_mate_ever a

    flush_mess2 ai a b'@(Just (ByQName _ bi b))
        | ai /= bi || qnames a /= qnames b = no_mate_ever a >> report' >> flush_mess1 b'
        | otherwise                        = fixmate a b    >>= yield >> report' >> flush_messed_up


    -- Flush the right_here queue.  Everything should come off in pairs,
    -- if not, it goes to messed_up.  When done, loop back to 'go'
    flush_here  r = fetchMin right_here' >>= flush_here1 r

    flush_here1 r Nothing = go' r
    flush_here1 r (Just a) = fetchMin right_here' >>= flush_here2 r a

    flush_here2 r (ByQName _ _ a) Nothing = do no_mate_here "flush_here2/Nothing" a
                                               flush_here r

    flush_here2 r (ByQName _ ai a) b'@(Just (ByQName _ bi b))
        | ai /= bi || qnames a /= qnames b = no_mate_here "flush_here2/Just" a >> flush_here1 r b'
        | otherwise                        = fixmate a b >>= yield >> flush_here r


    -- add stuff coming from 'in_order' to 'right_here'
    complete_here pivot = do
            zz <- peekMin in_order
            case zz of
                Nothing -> return ()
                Just (ByMatePos b)
                       | pivot  > br_mate_pos b -> do discardMin in_order'
                                                      no_mate_here "complete_here" b
                                                      complete_here pivot

                       | pivot == br_mate_pos b -> do discardMin in_order'
                                                      enqueue (byQName b) right_here'
                                                      complete_here pivot

                       | otherwise -> return ()

    finish () st (QS x y z) _cf _rs i = do
        liftIO $ closePQ x >> closePQ y >> closePQ z
        lift $ mapM_ logStringLn (report_stats st)
        lift $ Q.effects i


-- | To catch pairs whose mates are adjacent (either because the file
-- has never been sorted or because it has been group-sorted), we apply
-- preprocessing.  The idea is that if we can catch these pairs early,
-- the priority queues never fill up and we save a ton of processing.
-- Now to make the re-pair algorithm work well, we need to merge-sort
-- inputs.  But after that, the pairs have been separated.  So we apply
-- the preprocessing to each input file, then merge them, then run
-- re-pair.

data BamPair = Singleton BamRaw | Pair BamRaw BamRaw | LoneMate BamRaw | Other BamRaw

mergeInputs :: (MonadIO m, MonadLog m, MonadMask m) => [FilePath] -> (BamMeta -> Q.Stream (Q.Of BamPair) m () -> m r) -> m r
mergeInputs [] k = decodeBam (streamHandle stdin) >>= \(hdr, strm) -> k hdr (quick_pair strm)
mergeInputs fs k = decodeBamFiles fs $ \bs ->
    k (foldMap fst bs)
      (foldr (\a b -> void $ mergeStreamsOn (bp_rname &&& bp_pos) (quick_pair $ snd a) b) (pure ()) bs)

quick_pair :: Monad m => Q.Stream (Q.Of BamRaw) m r -> Q.Stream (Q.Of BamPair) m r
quick_pair = go0
  where
    go0 = lift . Q.next >=> either pure (uncurry go1)

    go1 x s | isOther (unpackBam x)        = Q.cons (Other x) (go0 s)
            | not (isPaired (unpackBam x)) = Q.cons (Singleton x) (go0 s)
            | otherwise                    = lift (Q.next s) >>= either (Q.cons (LoneMate x) . pure) (uncurry $ go2 x)

    go2 x y s | isOther (unpackBam y)                          = Q.cons (Other y) (go1 x s)
              | b_qname (unpackBam x) == b_qname (unpackBam y) = Q.cons (Pair x y) (go0 s)
              | otherwise                                      = Q.cons (LoneMate x) (go1 y s)

-- auxillary (now called secondary) or supplementary
isOther :: BamRec -> Bool
isOther = liftA2 (||) isSecondary isSupplementary
{-# INLINE isOther #-}

bp_rname :: BamPair -> Refseq
bp_rname (Singleton u) = b_rname $ unpackBam u
bp_rname (Pair    u _) = b_rname $ unpackBam u
bp_rname (LoneMate  u) = b_rname $ unpackBam u
bp_rname (Other     u) = b_rname $ unpackBam u
{-# INLINE bp_rname #-}

bp_pos :: BamPair -> Int
bp_pos (Singleton u) = b_pos $ unpackBam u
bp_pos (Pair    u _) = b_pos $ unpackBam u
bp_pos (LoneMate  u) = b_pos $ unpackBam u
bp_pos (Other     u) = b_pos $ unpackBam u
{-# INLINE bp_pos #-}


data ByQName = ByQName { _bq_hash :: !Int
                       , _bq_alnid :: !Int
                       , _bq_rec :: !BamRaw }

-- Note on XI:  this is *not* the index read (MPI EVAN convention), but
-- the number of the alignment (Segemehl convention, I believe).
-- Fortunately, the index is never numeric, so this is reasonably safe.
byQName :: BamRaw -> ByQName
byQName b = ByQName (hash $ qnames b) (extAsInt 0 "XI" $ unpackBam b) b

instance Eq ByQName where
    ByQName ah ai a == ByQName bh bi b =
        (ah, ai, qnames a) == (bh, bi, qnames b)

instance Ord ByQName where
    ByQName ah ai a `compare` ByQName bh bi b =
        (ah, ai, b_qname (unpackBam a)) `compare` (bh, bi, b_qname (unpackBam b))

instance Extern ByQName   where
    usedBytes (ByQName _ _ r) = S.length (raw_data r) + 10 * sizeOf (0::Int)
    extern    (ByQName _ _ r) = put_byte_string (raw_data r)
    intern                    = liftM byQName . bamRaw 0 =<< get_byte_string



newtype ByMatePos = ByMatePos BamRaw

br_mate_pos :: BamRaw -> (Refseq, Int)
br_mate_pos = (b_mrnm &&& b_mpos) . unpackBam

instance Eq ByMatePos where
    ByMatePos a == ByMatePos b =
        br_mate_pos a == br_mate_pos b

instance Ord ByMatePos where
    ByMatePos a `compare` ByMatePos b =
        br_mate_pos a `compare` br_mate_pos b

instance Extern ByMatePos where
    usedBytes (ByMatePos   r) = S.length (raw_data r) +  8 * sizeOf (0::Int)
    extern    (ByMatePos   r) = put_byte_string (raw_data r)
    intern                    = liftM ByMatePos . bamRaw 0 =<< get_byte_string

