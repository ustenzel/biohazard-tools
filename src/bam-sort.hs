-- This sorts a number of Bam files by coordinate.  It's not exactly
-- fast and usually provides little benefit compared to samtools.
-- However, it serves as a stress test for the externally backed
-- PriorityQueue module; it should work under all circumstances (lots of
-- input, tight memory, spill to thousands of files), and never run out
-- of memory or file descriptors or slow down to impractical levels by
-- merging impractical numbers of files.
--
-- Since this is not generally useful (yet?), we only compile this as a
-- test suite and don't bother installing it.
--
-- One way this could become useful is if it provided a sort that was
-- efficient when applied to the \"almost sorted\" output of bam-fixpair
-- (or a similar program, or its postprocessed output).  The idea would
-- be to copy everything to a temporary file that follows the sort
-- order.  Those records that don't fit the normal order go to a
-- 'PriorityQueue'; at the end, we unfold the 'PriorityQueue' and merge
-- with the temporary stream.  If this avoids most of the merging work
-- (and keeps the number of streams to be read from low), it should be
-- faster than a general sort.  It implicitly reverts to the general
-- sort if the input is completely unsorted.
--
-- In a quick test, this opportunistic sort is almost twice as fast as
-- the general version on the output of bam-fixpair (with few actual
-- paired-end reads).  Is that worth it?
--
-- The opportunistic sort could also buffer a fixed amount of reads in
-- memory, perhaps using a separate 'SkewHeap', and only begin to stream
-- out once memory gets tight.  That way, reads that need to be moved
-- back only a little bit don't need to hit the disk.  XXX

import Bio.Bam
import Bio.Prelude                       hiding ( foldM_ )
import Control.Monad.Log
import Data.PriorityQueue.External
import Paths_biohazard_tools                    ( version )
import Bio.Streaming.Prelude                    ( foldM_ )
import System.Directory                         ( removeFile )
import System.IO                                ( openBinaryTempFile )

import qualified Data.ByteString as S

pqconf :: PQ_Conf LIO
pqconf = PQ_Conf 300 25 "/var/tmp/" logStringLn

-- Usage:
--   bam-sort 1 out.bam [in.bam ...]
--     Reads the input files in sequence, sorts the contents, writes
--     them to out.bam.  Effectively internal heap sort followed by
--     external merge sort.
--
--   bam-sort 2 out.bam [in.bam ...]
--     Merges the input files, writes the records that appear in the
--     correct order relative to their predecessor to one file, sorts
--     all other records, writes them to out.bam.  A slight improvement
--     over ordinary sorting.

loggingConf :: LoggingConf
loggingConf = LoggingConf { reporting_level = Info
                          , logging_level   = Warning
                          , error_level     = Error
                          , max_log_size    = 10
                          , want_progress   = True }

main :: IO ()
main = getArgs >>= \case
    "1" : out : files -> withLogging_ loggingConf (main_merge_sort out files)
    "2" : out : files -> withLogging_ loggingConf (main_one_queue  out files)
    _                 -> pure ()


main_merge_sort :: String -> [String] -> LIO ()
main_merge_sort out files = do
    (hdr,pq) <- concatInputs files $ \h ->
                  foldM_ (\pq br -> enqueuePQ pqconf (BySelfPos br) pq)
                         (return makePQ) (\r -> return (h,r)) .
                  progressNum "read" 1000000

    logStringLn "Input phase done, writing output."
    add_pg <- liftIO $ addPG $ Just version

    writeBamFile out (add_pg $ hdr { meta_hdr = (meta_hdr hdr) { hdr_sorting = Coordinate } }) $
        progressNum "wrote" 1000000 $ streamPQ pq ()


main_one_queue :: String -> [String] -> LIO ()
main_one_queue out files = do
    path <- fromMaybe "/var/tmp" <$> liftIO (lookupEnv "TEMP")
    bracket ( liftIO $ openBinaryTempFile path "bam-sort.bam" )
            ( \(fp,hdl) -> liftIO $ hClose hdl >> removeFile fp )
            $ \(_fp,htmp) -> do
        (hdr, pq) <- mergeInputsOn coordinates files $ \hdr bs ->
                        fmap ((,) hdr) $
                        writeBamHandle htmp hdr $
                        isolate_sorted_part $
                        progressNum "read" 1000000 bs

        logStringLn "Input phase done, writing output."
        add_pg <- liftIO $ addPG $ Just version

        liftIO $ hFlush htmp
        liftIO $ hSeek htmp AbsoluteSeek 0
        (_, bs) <- decodeBam $ streamHandle htmp
        writeBamFile out (add_pg $ hdr { meta_hdr = (meta_hdr hdr) { hdr_sorting = Coordinate } })
            $ progressNum "wrote" 1000000 $ mergePQ pq bs


streamPQ :: MonadIO m => PQ BySelfPos -> b -> Stream (Of BamRaw) m b
streamPQ pq r = case viewMinPQ pq of
    Nothing                 -> pure r
    Just (BySelfPos a, pq') -> wrap $ a :> (liftIO pq' >>= flip streamPQ r)

mergePQ :: MonadIO m => PQ BySelfPos -> Stream (Of BamRaw) m b -> Stream (Of BamRaw) m b
mergePQ pq0 = lift . inspect >=> either (streamPQ pq0) (merge1 pq0)
  where
    merge1 pq (a :> s) =
        case viewMinPQ pq of
            Nothing -> wrap (a :> s)
            Just (BySelfPos b,pq')
                | coordinates a <= coordinates b -> wrap $ a :> mergePQ pq s
                | otherwise                      -> wrap $ b :> (liftIO pq' >>= flip merge1 (a :> s))


-- How to make it opportunistic?  We need a PriorityQueue and a stream.
-- It's probably easiest if the stream is a temporary Bam file.  What
-- comes in goes into the stream unless its coordinate is smaller than
-- that of the previous record.  Else it goes to the PQ.
--
-- We take a 'Stream' and return the sorted subset.  Everything
-- else is accumulated in the PQ.  As the final value, we return the PQ.

isolate_sorted_part :: Stream (Of BamRaw) LIO () -> Stream (Of BamRaw) LIO (PQ BySelfPos)
isolate_sorted_part = lift . inspect >=> ini
  where
    ini (Left         ()) = pure makePQ
    ini (Right (br :> s)) = wrap $ br :> go makePQ (coordinates br) s

    go pq pos = lift . inspect >=> \case
        Left () -> pure pq
        Right (br :> s)
            | coordinates br >= pos -> wrap $ br :> go pq (coordinates br) s
            | otherwise -> do pq' <- lift $ enqueuePQ pqconf (BySelfPos br) pq
                              go pq' pos s


newtype BySelfPos = BySelfPos BamRaw

instance Eq BySelfPos where
    BySelfPos a == BySelfPos b =
        coordinates a == coordinates b

instance Ord BySelfPos where
    BySelfPos a `compare` BySelfPos b =
        coordinates a `compare` coordinates b

instance Extern BySelfPos where
    usedBytes (BySelfPos   r) = S.length (raw_data r) +  8 * sizeOf (0::Int)
    extern    (BySelfPos   r) = put_byte_string (raw_data r)
    intern                    = liftM BySelfPos . bamRaw 0 =<< get_byte_string

