{-# LANGUAGE TemplateHaskell #-}
import Bio.Bam
import Bio.Prelude
import Bio.Util.Git
import Paths_biohazard_tools                ( version )
import Options.Applicative

import qualified Bio.Streaming.Prelude      as Q

data Conf = Conf { c_output    :: BamMeta -> Stream (Of (Either BamRaw BamRec)) LIO () -> LIO ()
                 , c_trim_pred :: [Nucleotides] -> [Qual] -> Bool
                 , c_pass_pred :: BamRec -> Bool
                 , c_files     :: [FilePath] }

optionsA :: Parser Conf
optionsA = Conf
    <$> (writeBamFile <$> strOption (short 'o' <> long "output" <> help "Write output to FILE" <> metavar "FILE")
              <|> pure (\m -> pipeBamOutput m . protectTerm))
    <*> (trim_low_quality . Q <$> option auto
          (short 'q' <> long "minq" <> metavar "Q" <> help "Trim where quality is below Q"))
    <*> (flag isMerged (liftA2 (||) isMerged isUnmapped)
          (short 'm' <> long "mapped" <> help "Trim only mapped sequences"))
    <*> many (strArgument (metavar "FILE"))


main :: IO ()
main =
    execWithParser_ optionsA (Just version) (giFullVersion $$gitInfo)
                    (fullDesc <> header "Simple trimming of sequences in Bam files"
                              <> progDesc "Reads Bam files, trims sequences of low quality, \
                                          \writes Bam to stdout.  Does not trim merged reads.") $ \c -> do

    let do_trim r | c_pass_pred c r' = Left r
                  | otherwise        = Right $ trim_3' (c_trim_pred c) r'
            where r' = unpackBam r

    add_pg <- liftIO $ addPG (Just version)
    concatInputs (c_files c) $ \hdr ->
        c_output c (add_pg hdr) . Q.map do_trim

