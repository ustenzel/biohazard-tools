{-# LANGUAGE TemplateHaskell #-}
import Bio.Prelude
import Bio.Util.Git
import Dir.Bam
import Options.Applicative
import Paths_biohazard_tools                        ( version )
import System.Directory                             ( removeFile )
import System.IO.Error                              ( tryIOError )

import qualified Bio.Streaming.Prelude         as Q
import qualified Dir.PTrieAdt                  as PTA
import qualified Dir.PTrieFinal                as PTF
import qualified Dir.PTrieStream               as PTS

options :: Parser (LIO ())
options = hsubparser $
    command "index"  (info opt_index  (progDesc "Index a BAM file by read name")) <>
    command "lookup" (info opt_lookup (progDesc "Access a BAM file by read name"))

opt_index :: Parser (LIO ())
opt_index = main_index
    <$> optional (strOption (short 'o' <> long "output" <> metavar "FILE" <> help "Write index to FILE"))
    <*> option auto (value 0x300000 <> short 'c' <> long "chunk-length" <> metavar "NUM" <> help "Process NUM entries per temporary file")
    <*> option auto (value       16 <> short 'n' <> long "num-files"    <> metavar "NUM" <> help "Merge up to NUM files per pass")
    <*> strArgument (metavar "FILE" <> help "BAM file to index")
  where
    main_index mofn clen nfs fn =
        let ofn = fromMaybe (fn ++ ".nmi") mofn in
        streamInput fn $
            decodeBam >=>
            build_tries clen ofn . progressNum "Indexing" 0x80000 . snd >=>
            merge_many nfs ofn

opt_lookup :: Parser (LIO ())
opt_lookup = main_lookup_
    <$> strOption (short 'f' <> long "file" <> metavar "FILE" <> help "BAM file to access")
    <*> optional (strOption (short 'i' <> long "index" <> metavar "FILE" <> help "Index is stored in FILE"))
    <*> many (strArgument (metavar "QNAME" <> help "Look for read names beginning with QNAME"))
  where
    main_lookup_ fn mifn names =
        let ifn = fromMaybe (fn ++ ".nmi") mifn in
        main_lookup fn ifn (map encodeBytes $ map fromString names)

main :: IO ()
main =
  execWithParser_ options (Just version) (giFullVersion $$gitInfo)
    (header "Indexes BAM files by read name and provides queries via read name." <> fullDesc) id


-- min number of generations needed is the smallest n such that nfs^n >= #files.
-- balanced merge then uses ceil(#files^(1/#gen)) files at a time.
merge_many :: MonadIO m => Int -> FilePath -> [FilePath] -> m ()
merge_many nfs final_name fs0 = do
    liftIO $ printf "Have %d files, can merge %d at a time, will use %d generations and merge %d at once.\n"
                    (length fs0) nfs ngen nfs'
    liftIO $ go 1 fs0
  where
    ngen = length $ takeWhile (< length fs0) $ iterate (nfs*) 1
    nfs' = head [ i | i <- [2..], i^ngen >= length fs0 ]

    go gen fs | null (drop nfs fs)  =  do hPrintf stderr "Finalizing %d files...\n" (length fs)
                                          withMany fs $ PTF.dumpToFile final_name . foldPairs >=> print
                                          PTF.withFile final_name $ \idx  ->
                                                withMany fs $ Q.effects . PTF.sanityCheck idx . foldPairs
                                          mapM_ (tryIOError . removeFile) fs
              | otherwise           =  merge_gen nfs' final_name gen fs >>= go (gen+1)

merge_gen :: Int -> FilePath -> Int -> [FilePath] -> IO [FilePath]
merge_gen nfs final_name gen = join $ go [] (0::Int) . length
  where
    go acc !_    !_   [ ]  = pure $ reverse acc
    go acc !_    !_   [f]  = pure $ reverse (f:acc)
    go acc !fnum !lfs ifs  =  do
            let (u,v) = splitAt nfs ifs
                lfs'  = max 0 (lfs - nfs)
            hPrintf stderr "Consolidating %d (+%d) files for generation %d...\n" (length u) lfs' gen
            let nm = printf "%s.%02d.%04d" final_name gen fnum
            withMany u $ PTS.dumpToFile nm . foldPairs
            mapM_ (tryIOError . removeFile) u
            go (nm:acc) (fnum+1) lfs' v

withMany :: [FilePath] -> ([PTS.PTrie Word32 IO ()] -> IO r) -> IO r
withMany [] k = k []
withMany (f:fs) k = PTS.withFile f $ \t -> withMany fs $ \ts -> k (t:ts)

foldPairs :: Semigroup a => [a] -> a
foldPairs [x] = x
foldPairs  xs = foldPairs $ go xs
  where
    go (x:y:zs) = (<>) x y : go zs
    go [  x   ] = [x]
    go [      ] = [ ]
{-# INLINE foldPairs #-}


build_tries :: (MonadLog m, MonadIO m) => Int -> FilePath -> Stream (Of BamRaw) m r -> m [FilePath]
build_tries maxrecs fp = go [] (0::Int)
  where
    go acc !n = Q.next >=> \case
        Left    _    -> pure $ reverse acc
        Right (x, s) -> do t0 :> s' <- Q.fold (\t y -> t <> build_trie y) (build_trie x) id $ Q.splitAt (maxrecs-1) s
                           let fp0 = printf "%s.%02d.%04d" fp (0::Int) n
                           case PTA.countNodes t0 of
                               cc :!: dd -> logStringLn $ printf "Dumping to %s: %d nodes, %d deep." fp0 cc dd
                           liftIO (PTS.dumpToFile fp0 (PTS.fromAdt t0))
                           go (fp0:acc) (n+1) s'



-- This keeps the top 30 bits of the virtual offset.  That is, we store
-- the offset of the current BZGF block at a resolution of 256k (18
-- bits) and the scheme is guaranteed to work as long as the ordinary
-- BAM indices work (i.e. up to 256T file size).
build_trie :: BamRaw -> PTA.PTrie Word32
build_trie br = PTA.singleton (b_qname . unpackBam $ br) (fromIntegral $ virt_offset br `shiftR` 34)
{-# INLINE build_trie #-}

main_lookup :: FilePath -> FilePath -> [Bytes] -> LIO ()
main_lookup bfn ifn names =
    bracket (liftIO $ openBinaryFile bfn ReadMode) (liftIO . hClose)    $ \hbam ->
    PTF.withFile ifn                                                    $ \idx  ->
    decodeBam (streamHandle hbam)                                     >>= \(hdr,_) ->
    pipeSamOutput hdr $ mapM_ (streamName hbam idx) names


