{-# LANGUAGE TemplateHaskell #-}
-- Re-wrap alignments to obey the given length of the reference
-- sequence.
--
-- The idea is that a circular reference sequence has been extended
-- artificially to facilitate alignment.  Now the declared length in the
-- header is wrong, and the alignments overhang the end.  Here we split
-- those alignments into two, one for the beginning, one for the end of
-- the sequence, then soft-mask out the inappropriate parts.
--
-- What's the best course of action, operationally?  As usual, we need
-- to decide whether to rely on sorted input and whether to produce
-- sorted output, and how much to copy senselessly.
--
-- In a sane world, this program runs precisely once, after alignment,
-- and output is piped somewhere.  So that's what we do:  input is
-- unsorted, so is output, output is piped (and hence uncompressed).
-- We also fix the header while we're at it.
--
-- We try to fix the map quality for the affected reads as follows:  if
-- a read has map quality 0 (meaning multiple equally good hits), we
-- check the XA field.  If it reports exactly one additional alignment,
-- and it matches the primary alignment when transformed to canonical
-- coordinates, we remove XA and set MAPQ to 37.

import Bio.Bam
import Bio.Bam.Rmdup                    ( normalizeTo, wrapTo )
import Bio.Prelude
import Bio.Util.Git
import Control.Monad.Log                ( panic )
import Paths_biohazard_tools            ( version )
import Options.Applicative

import qualified Bio.Streaming.Prelude  as Q
import qualified Data.ByteString.Char8  as S
import qualified Data.Map               as M

data Opts = Opts
    { output :: BamMeta -> Stream (Of (Either BamRaw BamRec)) LIO () -> LIO ()
    , args   :: [String] }

options :: Parser Opts
options = Opts
    <$> (writeBamFile <$> strOption (short 'o' <> long "output" <> help "Write output to FILE" <> metavar "FILE") <|>
         pure (\m -> pipeBamOutput m . protectTerm))
    <*> many (strArgument (metavar "FILE" <> help "Input from FILE"))
    -- *sigh*  This doesn't actually work (we fix it up later), but it produces the correct usage info.
    <*  many (argument disabled (metavar "CHROM:LENGTH" <> help "CHROM actually has length LENGTH"))

main :: IO ()
main = execWithParser_ options (Just version) (giFullVersion $$gitInfo)
                       (header "Wrap circularized reference sequences" <> fullDesc <>
                        progDesc "Transforms BAM files.  For every 'CHROM' mentioned on the command line, \
                                 \wraps alignments to a new target length of 'LENGTH'.") $ \Opts{..} -> do
       add_pg <- addPG (Just version)
       concatInputs (filter (not . is_circ_arg) args) $ \hdr bams -> do
          (ltab, seqs') <- parseArgs (meta_refs hdr) (filter is_circ_arg args)
          output (add_pg hdr { meta_refs = seqs' }) . Q.concat . Q.map (rewrap $ M.fromList ltab) $ bams
  where
    is_circ_arg = (==) 1 . length . filter ((==) ':')

parseArgs :: Refs -> [String] -> LIO ([(Refseq,(Int,Bytes))], Refs)
parseArgs (Refs refs) args | sizeofArray refs == 0  =  panic "no target sequences found (empty input?)"
                           | otherwise              =  do mr <- liftIO $ thawArray refs 0 (sizeofArray refs)
                                                          r  <- foldM (parseArg (Refs refs) mr) [] args
                                                          (,) r . Refs <$> liftIO (unsafeFreezeArray mr)
  where
    parseArg (Refs h) mr sqs arg = case break (==':') arg of
        (nm,':':r) -> case reads r of
            [(l,[])] | l > 0 -> case filter (S.isPrefixOf (fromString nm) . sq_name . snd) $ zip [0..] $ toList h of
                [(k,a)] | sq_length a >= l -> do liftIO $ writeArray mr k a {sq_length=l}
                                                 return $ (Refseq (fromIntegral k),(l,sq_name a)) : sqs
                        | otherwise -> panic $ "cannot wrap " ++ show nm ++ " to " ++ show l
                                            ++ ", which is more than the original " ++ show (sq_length a)
                [] -> panic $ "no match for target sequence " ++ show nm
                _ -> panic $ "target sequence " ++ show nm ++ " is ambiguous"
            _ -> panic $ "couldn't parse length " ++ show r ++ " for " ++ show nm
        _ -> panic $ "couldn't parse argument " ++ show arg


-- | This runs both stages of the rewrapping: First normalize alignments
-- (so that POS is in the canonical interval) and fix XA, MPOS, MAPQ where
-- appropriate, then duplicate the read and softmask the noncanonical
-- parts.  We ignore sorting in here.
rewrap :: M.Map Refseq (Int,Bytes) -> BamRaw -> [Either BamRaw BamRec]
rewrap m b = maybe [Left b] (uncurry go) $ M.lookup (b_rname $ unpackBam b) m
  where
    go l nm = map (either Right Right) . wrapTo l .
              either id id . normalizeTo nm l $
              unpackBam b
