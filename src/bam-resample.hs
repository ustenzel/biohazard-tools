-- Resample m out of n `virtual' BAM records.
--
-- Strategy for fair down sampling:  we first count the number of
-- records, then scan again to sample.  Input must be grouped by QNAME
-- (sorted by QNAME is fine).
--
-- Usage: resample [NUM] [FILE...]

import Bio.Bam
import Bio.Prelude
import Control.Monad.Log
import Paths_biohazard_tools ( version )
import System.Random         ( randomRIO )

import qualified Bio.Streaming.Prelude as Q

main :: IO ()
main = do
    args <- getArgs
    case args of
        []             -> complain
        [_num]         -> complain
        num_ : files   -> case reads num_ of
            [(num,"")] -> withLogging_ logconf (main' num files)
            _          -> complain
  where
    logconf = LoggingConf Warning Warning Error 20 True

complain :: IO ()
complain = do pn <- getProgName
              hPutStr stderr $ pn ++ ", version " ++ showVersion version
                            ++ "\nUsage: " ++ pn ++ " <num> [file...]\n"
              exitFailure

main' :: Int -> [String] -> LIO ()
main' num files = do
    liftIO $ hPutStr stderr "Counting... "
    total <- concatInputs files $ \_hdr ->
             Q.length_
             . mapped (fmap (() :>) <$> Q.effects)
             . Q.groupBy ((==) `on` qnames)

    liftIO $ hPutStr stderr $ shows total " records.\n"
    when (num > total) $ liftIO $ hPutStr stderr "Not enough data, I will copy the whole input.\n"

    add_pg <- liftIO $ addPG (Just version)
    concatInputs files $ \hdr ->
             pipeBamOutput (add_pg hdr)
             . protectTerm
             . resample num total
             . Q.groupBy ((==) `on` qnames)


resample :: MonadIO m => Int -> Int -> Q.Stream (Q.Stream (Of BamRaw) m) m a -> Q.Stream (Of BamRaw) m a
resample = go
  where
    go  !m !n    = lift . inspect >=> either pure (go' m n)
    go' !m !n as = do r <- liftIO $ randomRIO (0,n-1)
                      if r < m then                 as  >>= go (m-1) (n-1)
                               else lift (Q.effects as) >>= go  m    (n-1)
