module Mangle.Parse
    ( p_bool_expr
    , p_num_atom
    , p_string_atom)
  where

import Bio.Bam.Header                           ( BamKey, bamKey )
import Bio.Prelude                       hiding ( (:>) )
import Control.Monad.Combinators.Expr
import Mangle.Lang
import Text.LLParser                     hiding ( Parser )

import qualified Data.ByteString                    as B
import qualified Text.LLParser                      as LL

type Parser = LL.Parser Bytes

p_bool_expr :: Parser Expr_Bool
p_bool_expr = makeExprParser p_bool_atom
    [ [ InfixL (AndE <$ operator "&&") ]
    , [ InfixL (OrE  <$ operator "||") ] ]

-- | Boolean atoms:
-- - string comparisons (where tagged fields default to "")
-- - numeric comparisons (where tagged fields default to 0)
-- - definedness/isnum/isstring of a tag
-- - negation
-- - true and false
-- - the flags

p_bool_atom :: Parser Expr_Bool
p_bool_atom = asum
    [ pChar8 '(' *> pSpace *> p_bool_expr <* pChar8 ')' <* pSpace
    , Not          <$> (operator "!"          *> p_bool_atom)
    , Not          <$> (reserved "not"        *> p_bool_atom)
    , Is_Defined   <$> (reserved "defined"    *> p_field_name)
    , Is_Num       <$> (reserved "isnum"      *> p_field_name)
    , Is_String    <$> (reserved "isstring"   *> p_field_name)
    , IsDeaminated <$> (reserved "deaminated" *> pInt)

    , p_ambiguous_field <**> either_op
    , pTry $ identifier >>= fmap Predicate . named_predicate
    , p_string_atom <**> str_op <*> p_string_atom
    , p_num_atom    <**> num_op <*> p_num_atom ]
  where
    either_op :: Parser (AmbiguousField -> Expr_Bool)
    either_op = (\op y x -> op (FieldAsBytes  x) y) <$> str_op <*> p_string_atom1
            <|> (\op y x -> op (FieldAsDouble x) y) <$> num_op <*> p_num_atom1

    p_string_atom1 = p_string_atom <|> FieldAsBytes  <$> p_ambiguous_field
    p_num_atom1    = p_num_atom    <|> FieldAsDouble <$> p_ambiguous_field

    named_predicate :: Bytes -> Parser PredicateName
    named_predicate "true"         = pure PredTrue
    named_predicate "false"        = pure PredFalse

    named_predicate "paired"       = pure PredPaired
    named_predicate "properly"     = pure PredProperly
    named_predicate "unmapped"     = pure PredUnmapped
    named_predicate "mapped"       = pure PredMapped
    named_predicate "mate-unmapped"= pure PredMateUnmapped
    named_predicate "mate-mapped"  = pure PredMateMapped
    named_predicate "reversed"     = pure PredReversed
    named_predicate "mate-reversed"= pure PredMateReversed
    named_predicate "first-mate"   = pure PredFirstMate
    named_predicate "second-mate"  = pure PredSecondMate
    named_predicate "auxillary"    = pure PredAuxillary
    named_predicate "secondary"    = pure PredSecondary
    named_predicate "failed"       = pure PredFailed
    named_predicate "good"         = pure PredGood
    named_predicate "duplicate"    = pure PredDuplicate
    named_predicate "trimmed"      = pure PredTrimmed
    named_predicate "merged"       = pure PredMerged
    named_predicate "alternative"  = pure PredAlternative
    named_predicate "exact-index"  = pure PredExactIndex

    named_predicate "clear-failed" = pure PredClearFailed
    named_predicate "set-failed"   = pure PredSetFailed
    named_predicate "set-trimmed"  = pure PredSetTrimmed
    named_predicate "set-merged"   = pure PredSetMerged

    named_predicate key = fail $ unpack key ++ ": not a known predicate"

    num_op :: Parser (Expr_Double -> Expr_Double -> Expr_Bool)
    num_op = asum [ (:==) <$ operator "==", (:<=) <$ operator "<=", (:>=) <$ operator ">="
                  , (:/=) <$ operator "!=", (:<)  <$ operator  "<", (:>)  <$ operator  ">" ]

    str_op :: Parser (Expr_Bytes -> Expr_Bytes -> Expr_Bool)
    str_op = asum [ (:=~) <$ operator "~", (:/=~) <$ operator "!~" ]



-- | Numeric-valued atoms (float or int, everything is converted to
-- double).  This includes:
-- - predefined fields: POS, MPOS, ISIZE, LENGTH, MAPQ
-- - mnemonic constants: wrongness, unknownness, rgquality
-- - literals

p_num_atom :: Parser Expr_Double
p_num_atom = ( NumConst <$> float ) <|> ( identifier >>= fmap NumField . numeric_field )
  where
    numeric_field "POS"         = pure FieldPos
    numeric_field "MPOS"        = pure FieldMpos
    numeric_field "PNEXT"       = pure FieldPnext
    numeric_field "ISIZE"       = pure FieldIsize
    numeric_field "TLEN"        = pure FieldTlen
    numeric_field "MAPQ"        = pure FieldMapq
    numeric_field "LENGTH"      = pure FieldLen
    numeric_field "LEN"         = pure FieldLen

    numeric_field "unknownness" = pure FieldUnknownness
    numeric_field "rgquality"   = pure FieldRgquality
    numeric_field "wrongness"   = pure FieldWrongness

    numeric_field key = fail $ unpack key ++ ": not a known numeric field"


isNameChar_w8 :: Word8 -> Bool
isNameChar_w8 c = isAlpha_w8 c || isDigit_w8 c || c == c2w '-'

-- | Parses name of a tagged field.  This is an alphabetic character
-- followed by an alphanumeric character.
p_field_name :: Parser BamKey
p_field_name = pTry (bamKey <$> pTestByte isAlpha_w8
                            <*> pTestByte (\c -> isAlpha_w8 c || isDigit_w8 c)
                            <* (pNotFollowedBy isNameChar_w8 <?> "end of 2-letter tag"))
               <* pSpace
    <?> "2-letter tag"

-- | String-valued atoms.  This includes:
-- - convenience functions:  library, sample
-- - literals
p_string_atom :: Parser Expr_Bytes
p_string_atom = asum
    [ GetLibrary           <$  reserved "library"
    , GetSample            <$  reserved "sample"
    , GetQname             <$  reserved "QNAME"
    , Literal . fromString <$> pStringLit ]


-- | Fields that can reasonably be interpreted as either numeric or
-- strings.  These include the predefined fields RNAME, MRNM, RNEXT,
-- which can always be interpreted both ways, and tagged fields, which
-- can be one or the other according to their actual value.
p_ambiguous_field :: Parser AmbiguousField
p_ambiguous_field = asum
    [ NamedField <$> p_field_name
    , FieldRname <$  reserved "RNAME"
    , FieldRnext <$  reserved "MRNM"
    , FieldRnext <$  reserved "RNEXT" ]


operator :: Bytes -> Parser ()
operator nm = do pTry $ do pLiteral nm
                           pNotFollowedBy (`B.elem` ":!#$%&*+./<=>?@\\^|-~")
                               <?> ("end of " ++ show nm)
                 pSpace

reserved :: Bytes -> Parser ()
reserved nm = do pTry $ do pLiteral nm
                           pNotFollowedBy isNameChar_w8
                               <?> ("end of " ++ show nm)
                 pSpace

identifier :: Parser Bytes
identifier = pFollowedBy isAlpha_w8 *> pSpan isNameChar_w8 <* pSpace

float :: Parser Double
float = pFractional <* pSpace


