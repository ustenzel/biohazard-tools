module Mangle.Eval
    ( eval_Expr_Bool
    , eval_Expr_Double
    , eval_Expr_Bytes
    , interpret )
  where

import Bio.Adna                                 ( alnFromMd, npair )
import Bio.Bam                           hiding ( (:>) )
import Bio.Prelude                       hiding ( (:>) )
import Control.Monad.Trans.Reader               ( ReaderT, runReaderT, asks )
import Control.Monad.Trans.State.Strict         ( State, runState, gets, modify )
import Data.HashMap.Strict                      ( lookupDefault )
import GHC.Float                                ( float2Double )
import Mangle.Lang
import Text.Regex.Posix                         ( (=~) )

import qualified Data.ByteString                  as B
import qualified Data.Vector.Generic              as V

data Env = Env { libmap  :: HashMap Bytes Bytes
               , smpmap  :: HashMap Bytes Bytes
               , reflist :: Refs }

type Inter  = ReaderT Env (State BamRaw)

eval_Expr_Bool :: Expr_Bool -> Inter Bool
eval_Expr_Bool (AndE x y) = eval_Expr_Bool x >>= bool (pure False) (eval_Expr_Bool y)
eval_Expr_Bool (OrE  x y) = eval_Expr_Bool x >>= bool (eval_Expr_Bool y) (pure  True)
eval_Expr_Bool (Not    x) = not <$> eval_Expr_Bool x

eval_Expr_Bool (x :== y) = liftA2 (==) (eval_Expr_Double x) (eval_Expr_Double y)
eval_Expr_Bool (x :<= y) = liftA2 (<=) (eval_Expr_Double x) (eval_Expr_Double y)
eval_Expr_Bool (x :>= y) = liftA2 (>=) (eval_Expr_Double x) (eval_Expr_Double y)
eval_Expr_Bool (x :/= y) = liftA2 (/=) (eval_Expr_Double x) (eval_Expr_Double y)
eval_Expr_Bool (x :<  y) = liftA2 (<) (eval_Expr_Double x) (eval_Expr_Double y)
eval_Expr_Bool (x :>  y) = liftA2 (>) (eval_Expr_Double x) (eval_Expr_Double y)

eval_Expr_Bool (x :=~  y) = liftA2             (=~)  (eval_Expr_Bytes x) (eval_Expr_Bytes y)
eval_Expr_Bool (x :/=~ y) = liftA2 (fmap not . (=~)) (eval_Expr_Bytes x) (eval_Expr_Bytes y)

eval_Expr_Bool (Is_Num key) =
    lift . gets $ \br -> case lookup key (b_exts (unpackBam br)) of
            Just (Int   _) -> True
            Just (Float _) -> True
            _              -> False

eval_Expr_Bool (Is_String key) =
    lift . gets $ \br -> case lookup key (b_exts (unpackBam br)) of
            Just (Text _) -> True
            Just (Bin  _) -> True
            Just (Char _) -> True
            _             -> False

eval_Expr_Bool (Is_Defined key) =
    lift . gets $ isJust . lookup key . b_exts . unpackBam

eval_Expr_Bool (IsDeaminated n) =
    lift . gets $ \br ->
        case unpackBam br of { b ->
        case alnFromMd (b_seq b) (b_cigar b) <$> getMd b of
            Nothing  -> False
            Just aln -> V.any (== cg) (V.take    n  aln) ||
                        V.any (== cg) (V.drop (l-n) aln) ||
                        V.any (== ga) (V.drop (l-n) aln)
              where
                l  = V.length aln
                cg = npair nucsC nucsT
                ga = npair nucsG nucsA  }

eval_Expr_Bool (Predicate nm) = case nm of
    PredTrue         -> pure True
    PredFalse        -> pure False

    PredPaired       -> getF isPaired
    PredProperly     -> getF isProperlyPaired
    PredUnmapped     -> getF isUnmapped
    PredMapped       -> getF (not . isUnmapped)
    PredMateUnmapped -> getF isMateUnmapped
    PredMateMapped   -> getF (not . isMateUnmapped)
    PredReversed     -> getF isReversed
    PredMateReversed -> getF isMateReversed
    PredFirstMate    -> getF isFirstMate
    PredSecondMate   -> getF isSecondMate
    PredAuxillary    -> getF isSecondary
    PredSecondary    -> getF isSecondary
    PredFailed       -> getF isFailsQC
    PredGood         -> getF (not . isFailsQC)
    PredDuplicate    -> getF isDuplicate
    PredTrimmed      -> getF isTrimmed
    PredMerged       -> getF isMerged
    PredAlternative  -> getF isAlternative
    PredExactIndex   -> getF isExactIndex

    PredClearFailed  -> do_clearF
    PredSetFailed    -> do_setF
    PredSetTrimmed   -> setFF 1
    PredSetMerged    -> setFF 2
  where
    getF f = lift . gets $ f . unpackBam

    do_clearF = lift $ True <$ modify (\br ->
                    let f_hi = B.index (raw_data br) 15
                        rd'  = B.concat [ B.take 15 (raw_data br)
                                        , B.singleton (clearBit f_hi 1)
                                        , B.drop 16 (raw_data br) ]
                    in if testBit f_hi 1 then br { raw_data = rd' } else br)

    do_setF   = lift $ True <$ modify (\br ->
                    let f_hi = B.index (raw_data br) 15
                        rd'  = B.concat [ B.take 15 (raw_data br)
                                        , B.singleton (setBit f_hi 1)
                                        , B.drop 16 (raw_data br) ]
                    in if testBit f_hi 1 then br else br { raw_data = rd' })

    setFF f   = lift $ True <$ modify (\br ->
                    let b  = unpackBam br
                        ff = extAsInt 0 "FF" b
                        b' = b { b_exts = updateE "FF" (Int (ff .|. f)) $ b_exts b }
                    in if ff .|. f == ff then br else unsafePerformIO (packBam b'))


eval_Expr_Double :: Expr_Double -> Inter Double
eval_Expr_Double (NumConst  x) = pure x
eval_Expr_Double (NumField nm) = case nm of
    FieldPos         -> getF                b_pos
    FieldMpos        -> getF               b_mpos
    FieldPnext       -> getF               b_mpos
    FieldIsize       -> getF              b_isize
    FieldTlen        -> getF              b_isize
    FieldMapq        -> getF $ unQ .       b_mapq
    FieldLen         -> getF $ V.length .   b_seq

    FieldUnknownness -> getF $ extAsInt    0 "Z0"
    FieldRgquality   -> getF $ extAsInt 9999 "Z1"
    FieldWrongness   -> getF $ extAsInt    0 "Z2"
  where
    getF f = lift . gets $ fromIntegral . f . unpackBam

eval_Expr_Double (FieldAsDouble nm) = case nm of
    FieldRname     -> lookup_ref b_rname
    FieldRnext     -> lookup_ref b_mrnm
    NamedField key ->
        lift . gets $ \br -> case lookup key (b_exts (unpackBam br)) of
            Just (Int   x) -> fromIntegral x
            Just (Float x) -> float2Double x
            _              -> 0
  where
    lookup_ref f = lift . gets $ fromIntegral . unRefseq . f . unpackBam


eval_Expr_Bytes :: Expr_Bytes -> Inter Bytes
eval_Expr_Bytes e = case e of
    Literal bs -> pure bs
    GetQname   -> b_qname <$> lift (gets unpackBam)
    GetSample  -> do b <- lift $ gets unpackBam
                     let rg = extAsString "RG" b
                     asks $ lookupDefault rg rg . smpmap

    GetLibrary -> do b <- lift $ gets unpackBam
                     let lb = extAsString "LB" b
                     let rg = extAsString "RG" b
                     if not (B.null lb)
                       then pure lb
                       else asks $ lookupDefault rg rg . libmap

    FieldAsBytes FieldRname       -> lookup_ref b_rname
    FieldAsBytes FieldRnext       -> lookup_ref b_mrnm
    FieldAsBytes (NamedField key) ->
        lift . gets $ \br -> case lookup key (b_exts (unpackBam br)) of
            Just (Text  x) -> x
            Just (Bin   x) -> x
            Just (Char  x) -> B.singleton x
            _              -> B.empty
  where
    lookup_ref f = do m <- asks reflist
                      lift . gets $ sq_name . getRef m . f . unpackBam


interpret :: Inter a -> BamMeta -> BamRaw -> (a, BamRaw)
interpret e m = runState (runReaderT e (Env libs smps (meta_refs m)))
  where
    !libs = fromList [ (rg1, lb1)
                     | ("RG", stuff) <- meta_other_shit m
                     , rg1 <- take 1 [ rg | ("ID", rg) <- stuff ]
                     , lb1 <- take 1 [ lb | ("LB", lb) <- stuff ] ]

    !smps = fromList [ (rg1, sm1)
                     | ("RG", stuff) <- meta_other_shit m
                     , rg1 <- take 1 [ rg | ("ID", rg) <- stuff ]
                     , sm1 <- take 1 [ sm | ("SM", sm) <- stuff ] ]

