module Mangle.Lang where

import Bio.Bam.Header   ( BamKey )
import Data.ByteString  ( ByteString )
import Prelude

-- defunctionized version of the direct interpretion that used to be in
-- bam-mangle

data PredicateName
    = PredTrue
    | PredFalse

    | PredPaired
    | PredProperly
    | PredUnmapped
    | PredMapped
    | PredMateUnmapped
    | PredMateMapped
    | PredReversed
    | PredMateReversed
    | PredFirstMate
    | PredSecondMate
    | PredAuxillary
    | PredSecondary
    | PredFailed
    | PredGood
    | PredDuplicate
    | PredTrimmed
    | PredMerged
    | PredAlternative
    | PredExactIndex

    | PredClearFailed
    | PredSetFailed
    | PredSetTrimmed
    | PredSetMerged

data Expr_Bool
    = AndE           Expr_Bool Expr_Bool
    | OrE            Expr_Bool Expr_Bool
    | Not            Expr_Bool

    | Expr_Double :== Expr_Double
    | Expr_Double :<= Expr_Double
    | Expr_Double :>= Expr_Double
    | Expr_Double :/= Expr_Double
    | Expr_Double :<  Expr_Double
    | Expr_Double :>  Expr_Double

    | Expr_Bytes :=~  Expr_Bytes
    | Expr_Bytes :/=~ Expr_Bytes

    | Is_Defined    !BamKey
    | Is_Num        !BamKey
    | Is_String     !BamKey
    | IsDeaminated  !Int
    | Predicate     !PredicateName

data NumFieldName
    = FieldPos
    | FieldMpos
    | FieldPnext
    | FieldIsize
    | FieldTlen
    | FieldMapq
    | FieldLen

    | FieldUnknownness
    | FieldRgquality
    | FieldWrongness

data Expr_Double
    = NumConst      !Double
    | NumField      !NumFieldName
    | FieldAsDouble !AmbiguousField

data Expr_Bytes
    = Literal      !ByteString
    | FieldAsBytes !AmbiguousField
    | GetSample
    | GetQname
    | GetLibrary

data AmbiguousField
    = NamedField !BamKey
    | FieldRname
    | FieldRnext

