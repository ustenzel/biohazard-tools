{-# LANGUAGE TemplateHaskell #-}
-- Generic bam mangler.  Mostly filters, some mutating operations, some
-- extraction of data.  Simple expression language, comparable to Unix
-- 'find', but not to 'awk'.

import Bio.Bam                           hiding ( (:>) )
import Bio.Prelude
import Bio.Util.Git
import Mangle.Eval
import Mangle.Lang
import Mangle.Parse
import Options.Applicative
import Paths_biohazard_tools                    ( version )
import Text.LLParser                     hiding ( Parser )

import qualified Bio.Streaming.Prelude            as Q
import qualified Data.ByteString                  as B

data Conf = Conf
    { conf_output :: BamMeta -> Stream (Of BamRaw) LIO () -> LIO ()
    , conf_expr   :: Expr_Bool
    , conf_limit  :: Maybe Int
    , conf_input  :: [FilePath] }

options :: Parser Conf
options = Conf
    <$> (writeBamFile <$> strOption
            (short 'o' <> long "output" <> metavar "FILE" <> help "Send output to FILE") <|>
         flag' pipeSamOutput
            (short 'S' <> long "sam-pipe" <> help "Send sam to stdout") <|>
         option (parse_str p_print)
            (short 'p' <> long "print" <> metavar "EXPR" <> help "Print result of EXPR for each record") <|>
         option (parse_str p_sum)
            (short 's' <> long "sum" <> metavar "EXPR" <> help "Sum results of EXPR") <|>
         pure (\m -> pipeBamOutput m . protectTerm))
    <*> (option (parse_str p_bool_expr)
            (short 'e' <> long "expr" <> long "filter" <> metavar "EXPR" <> help "Use EXPR as filter") <|>
         argument (parse_str p_bool_expr) (metavar "EXPR"))
    <*> optional (option auto
            (short 'n' <> long "numout" <> long "limit" <> metavar "NUM" <> help "Output at most NUM records"))
    <*> many (strArgument (metavar "FILE"))
  where
    parse_str p = eitherReader $ bimap (show . fst) fst . pRun (pSpace *> p <* pEndOfString) . fromString
    p_sum   = (\f m -> Q.fold_ (\a -> (+) a . fst . interpret (eval_Expr_Double f) m) 0 id >=> liftIO . print)    <$> p_num_atom
    p_print = (\f m -> Q.mapM_ $ liftIO . B.hPut stdout . (`B.snoc` 10) . fst . interpret (eval_Expr_Bytes f) m) <$> p_string_atom <|>
              (\f m -> Q.mapM_ $ liftIO . print                         . fst . interpret (eval_Expr_Double f) m) <$> p_num_atom


main :: IO ()
main = execWithParser_ options (Just version) (giFullVersion $$gitInfo)
                       (fullDesc <> header "Filter BAM files by applying an expression to each record") $ \conf -> do
       add_pg <- addPG $ Just version
       concatInputs (conf_input conf) $ \meta ->
           conf_output conf (add_pg meta) .
           maybe id Q.take (conf_limit conf) .
           Q.mapMaybe (runExpr (conf_expr conf) meta)


runExpr :: Expr_Bool -> BamMeta -> BamRaw -> Maybe BamRaw
runExpr e m = unp . interpret (eval_Expr_Bool e) m
  where
    unp (True,  s) = Just s
    unp (False, _) = Nothing


