{-# LANGUAGE CPP #-}
-- | A PATRICIA trie intended to index BAM files by name.
--
-- The minimalistic version without any optimizations needs about 10GB
-- of RAM to store 30M nodes.  This is obviously impractical, but it's a
-- start.

module Dir.PTrieAdt
    ( PTrie(..)
    , union
    , singleton
    , walk
    , countNodes

    , merge_u
    ) where

import Bio.Prelude               hiding ( find )
import Dir.BitString
import qualified Data.ByteString as B

-- | We store paths up to 64 bits inline, to avoid the overhead of
-- storing small arrays everywhere.  Longer paths are represented using
-- multiple 'Stem' segments.

data PTrie a  =  Stem  !Word64 !Int !(PTrie a)           -- bits, number of bits
              |  Tip   !a [a]                            -- payload (a non-empty list)
              |  Split !(PTrie a) !(PTrie a)             -- split at the first bit
  deriving Show

-- Smart constructor.  Ensures that that there is no empty 'Stem'.
stem :: Word64 -> Int -> PTrie a -> PTrie a
stem _ 0 t = t
stem k l t = Stem k l t
{-# INLINE stem #-}

instance Ord a => Semigroup (PTrie a) where (<>) = unionPtries

singleton :: Bytes -> Word32 -> PTrie Word32
singleton !k0 !v | v .&. 0xC0000000 /= 0  =  error "Don't be greedy."
                 | otherwise              =  go 0 0 k0
  where
    go !a !l !k = case B.uncons k of
        Nothing                  ->  Stem a l $ Tip v []
        Just (c,cs) | l  ==  64  ->  Stem a 64 $ go (fromIntegral c `shiftL` 56) 8 cs
                    | otherwise  ->  go (a .|. fromIntegral c `shiftL` (56-l)) (l+8) cs
{-# INLINABLE singleton #-}

unionPtries :: Ord a => PTrie a -> PTrie a -> PTrie a
unionPtries (Tip      a as) (Tip      b bs)  =  uncurry Tip $ merge_u a as b bs
unionPtries (Tip      a as) (Split     u v)  =  Split (unionPtries (Tip a as) u) v
unionPtries (Tip      a as) (Stem    k l t)  =  union_tip_stem a as k l t
unionPtries (Split     x y) (Tip      b bs)  =  Split (unionPtries x (Tip b bs)) y
unionPtries (Split     x y) (Split     u v)  =  Split (unionPtries x u) (unionPtries y v)
unionPtries (Split     x y) (Stem    k l t)  =  union_split_stem x y k l t
unionPtries (Stem    k l t) (Tip      b bs)  =  union_tip_stem b bs k l t
unionPtries (Stem    k l t) (Split     u v)  =  union_split_stem u v k l t
unionPtries (Stem k1 l1 t1) (Stem k2 l2 t2)  =  union_stem_stem k1 l1 t1 k2 l2 t2

{-# INLINE union_split_stem #-}
union_split_stem :: Ord a => PTrie a -> PTrie a -> Word64 -> Int -> PTrie a -> PTrie a
union_split_stem x y k l t
    | testBit k 63  =  Split x (unionPtries y (stem (shiftL k 1) (l-1) t))
    | otherwise     =  Split (unionPtries x (stem (shiftL k 1) (l-1) t)) y

{-# INLINE union_tip_stem #-}
union_tip_stem :: Ord a => a -> [a] -> Word64 -> Int -> PTrie a -> PTrie a
union_tip_stem a as k l t
    | k == 0     =  Stem k l (unionPtries (Tip a as) t)
    | otherwise  =  stem 0 l1 $ Split (Tip a as) (stem (shiftL k (l1 + 1)) (l-l1-1) t)
  where
    l1 = countLeadingZeros k

{-# INLINE union_stem_stem #-}
union_stem_stem :: Ord a => Word64 -> Int -> PTrie a -> Word64 -> Int -> PTrie a -> PTrie a
union_stem_stem k1 l1 t1 k2 l2 t2
    | lm ==  0 && testBit k2 63  =  Split (stem (shiftL k1 1) (l1-1) t1) (stem (shiftL k2 1) (l2-1) t2)
    | lm ==  0                   =  Split (stem (shiftL k2 1) (l2-1) t2) (stem (shiftL k1 1) (l1-1) t1)
    | lm == 64                   =  Stem k1 64 $ unionPtries t1  t2
    | otherwise                  =  stem k1 lm $ unionPtries (stem (shiftL k1 lm) (l1-lm) t1) (stem (shiftL k2 lm) (l2-lm) t2)
  where
    lm  = countLeadingZeros (xor k1 k2) `min` l1 `min` l2

merge_u :: Ord a => a -> [a] -> a -> [a] -> (a,[a])
merge_u = merge_0
  where
    merge_0 a as b bs | a < b      =  (a, merge_a   as b bs)
                      | a > b      =  (b, merge_b a as   bs)
                      | otherwise  =  (a, merge_2   as   bs)

    merge_a [    ] b bs  =  b:bs
    merge_a (a:as) b bs  =  uncurry (:) $ merge_0 a as b bs

    merge_b a as [    ]  =  a:as
    merge_b a as (b:bs)  =  uncurry (:) $ merge_0 a as b bs

    merge_2 [    ]   bs    =  bs
    merge_2   as   [    ]  =  as
    merge_2 (a:as) (b:bs)  =  uncurry (:) $ merge_0 a as b bs
{-# INLINABLE merge_u #-}


countNodes :: PTrie a -> Pair Int Int
countNodes (Tip   _ as)  =  1 + length as :!: 1
countNodes (Split  x y)  =  case countNodes x of { cx :!: dx ->
                            case countNodes y of { cy :!: dy ->
                            cx+cy :!: max dx dy }}
countNodes (Stem _ _ t)  =  case countNodes t of { ct :!: dt ->
                            ct :!: 1+dt }

walk :: Show a => PTrie a -> [String]
walk = go emptyBS
  where
    go p (Stem k l t) = [ show l ++ " " ++ show p' ] ++
                        zipWith (++) ("`-":repeat "  ") (go p' t)
        where p' = appendBits p k l

    go p (Split  x y) = zipWith (++) ("+-":repeat "| ") (go (appendBit p False) x) ++
                        zipWith (++) ("`-":repeat "  ") (go (appendBit p True) y)

    go _ (Tip   a as) = [ show (a:as) ]

