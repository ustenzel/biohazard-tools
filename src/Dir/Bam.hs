module Dir.Bam
    ( module Bio.Bam
    , syncBgzf
    , syncBam
    , streamName
    ) where

import Bio.Bam
import Bio.Prelude hiding ( find )
import Dir.PTrieFinal ( PTrie, find )

import qualified Bio.Streaming.Bytes            as S
import qualified Bio.Streaming.Parse            as Q ( parseIO )
import qualified Bio.Streaming.Prelude          as Q
import qualified Data.ByteString                as B

-- | Looks for a BGZF header, starts decoding once found.  There is a tiny
-- chance (one in thirty billion or so) that it picks up something that
-- merely looks like a header, and fails spectacularly.  For
-- Bioinformatics, that's acceptable odds.  Stops trying after stream
-- position @eoff@.

syncBgzf :: MonadIO m => Int64 -> ByteStream m () -> ByteStream m ()
syncBgzf eoff = lift . S.unconsChunkOff >=> \case
    Left       () -> pure ()
    Right (c,o,s) -> go c o s
  where
    go !c !o | o >= eoff        =  const $ pure ()
             | B.length c < 18  =  lift . S.unconsChunkOff >=> \case
                                        Left         () -> pure ()
                                        Right (c1,_,s1) -> go (c <> c1) o s1
             | is_bgzf_hdr c    =  S.gunzip . S.consChunkOff c o
             | otherwise        =  go (B.tail c) (o+1)

    is_bgzf_hdr c = and [ B.index c  0 == 31
                        , B.index c  1 == 139
                        , B.index c 10 == 6
                        , B.index c 11 == 0
                        , B.index c 12 == 66
                        , B.index c 13 == 67
                        , B.index c  3 .&. 4 == 4 ]

-- | Looks for the string @name@.  Once found, it is assumed to be
-- the qname of a bam record.  A few invariants of the BAM record are
-- tested, to avoid tripping over random text in headers or tagged
-- fields.  Starting with this record and ending at stream position
-- @eoffset@, the stream of bam records is decoded.  If @name@ is not
-- found before position @eoffset@, an empty stream is produced.
--
-- * l_read_name is at offset 12.  There should be a NUL at offset
--   36+l_read_name-1.  This won't happen in header text or random tags,
--   so we should be safe.
-- * block_size is 'limited'.  What's a reasonable record size to check
--   against?  2^24?  So the byte at offset 3 should be NUL.
--
-- We need to keep 36 bytes, because they appear in BAM records before
-- the qname.  When extending the chunk, we additionally need to keep
-- as many bytes as @name@ is long.

syncBam :: MonadIO m => Int64 -> Bytes -> ByteStream m () -> Stream (Of BamRaw) m ()
syncBam eoffset name = lift . S.unconsChunkOff >=> \case
    Left       () -> pure ()
    Right (c,o,s) -> go c o s
  where
    go !c !o
        | o >= eoffset           = const $ pure ()

        -- This makes sure the record header and the name we're looking
        -- for is in the window.  If there isn't enough data to do that,
        -- we're done.
        | B.length c < 36 + B.length name
            = lift . S.unconsChunkOff >=> \case
                Left         () -> pure ()
                Right (c1,_,s1) -> go (c <> c1) o s1

        -- This makes sure the whole name (maximum length is 255) is in
        -- the window.  We're not yet done if we can't ensure this; the
        -- actual name may be shorter.
        | B.length c < 35 + 255
            = lift . S.unconsChunkOff >=> \case
                Left         () -> go_check c o (pure ())
                Right (c1,_,s1) -> go (c <> c1) o s1

        | otherwise = go_check c o

    go_check !c !o = uncurry (check c o) $ B.breakSubstring name (B.drop 36 c)

    check !c !o !u !v
        | good       =  Q.takeWhile ((< eoffset) . virt_offset) .
                        Q.unfoldr (Q.parseIO getBamRaw) .
                        S.consChunkOff (B.drop good_skip c) (o + fromIntegral good_skip)
        | otherwise  =  go (B.drop bad_skip c) (o + fromIntegral bad_skip)
      where
        bad_skip  = B.length c - 35 - B.length name
        good_skip = B.length u

        name_len = fromIntegral $ B.index c (good_skip + 12)
        good = not (B.null v) && B.index c (good_skip + 3) == 0 &&      -- found, and 'reasonable' block_size
               good_skip + 35 + name_len < B.length c &&                -- NUL byte inside the window
               B.index c (good_skip + 35 + name_len) == 0               -- NUL byte after qname


data LargeResult = LargeResult Bytes Int deriving Show

instance Exception LargeResult where
    displayException (LargeResult nm d) =
        printf "Lookup of %s may yield large result of about 2^%d entries." (show nm) d


-- | Looks up a name in an index, then streams all matching records.
streamName :: (MonadIO m, MonadLog m) => Handle -> PTrie -> Bytes -> Stream (Of BamRaw) m ()
streamName hbam idx name = do
    let warn d = when (d > 10) $ logMsg Warning (LargeResult name d)
    offsets <- lift $ map fromIntegral <$> find warn idx name
    forM_ (ranges offsets) $ \(offset, eoffset) -> do
        lift $ logStringLn $ printf "trying from %d (%d) to %d (%d)\n"
                                    (shiftL offset 18) offset (shiftL eoffset 18) eoffset
        liftIO $ hSeek hbam AbsoluteSeek $ fromIntegral $ shiftL offset 18

        Q.filter (B.isPrefixOf name . b_qname . unpackBam) $
            syncBam (shiftL eoffset 34) name $
            syncBgzf (shiftL eoffset 18) $
            S.hGetContentsN blocksize hbam
  where
    -- blocksize chosen so we typically need only one block
    blocksize = 0x60000

    ranges [] = []
    ranges (x:xs) = ranges1 x (x+1) xs

    ranges1 x y [    ]             = [(x,y)]
    ranges1 x y (z:zs) | z == y    = ranges1 x (y+1) zs
                       | otherwise = (x,y) : ranges (z:zs)

