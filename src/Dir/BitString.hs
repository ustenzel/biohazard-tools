module Dir.BitString
    ( BitString(..)
    , emptyBS
    , appendBit
    , appendBits
    ) where

import Bio.Prelude

-- | Lightweight bit string.  Used to traverse tries, mostly for debugging.
data BitString = BS { bs_length     :: !Int
                    , bs_final_word :: !Word8
                    , bs_rev_words  :: [Word8] }

emptyBS :: BitString
emptyBS = BS 0 0 []

appendBit :: BitString -> Bool -> BitString
appendBit (BS l w ws) b
    | l .&. 7 == 7  =  BS (l+1) 0 (w':ws)
    | otherwise     =  BS (l+1) w' ws
  where
    w' = if b then setBit w (7 - (l .&. 7)) else w

appendBits :: BitString -> Word64 -> Int -> BitString
appendBits !s !w !n
    | n   ==  0  =  s
    | otherwise  =  appendBits (appendBit s (testBit w 63)) (shiftL w 1) (n-1)

instance Show BitString where
    showsPrec _ (BS l w ws) =
        (++) "BS " .
        shows l .
        (++) " " .
        shows (map w2c $ reverse ws) .
        (++) "-" .
        fmtBin (l .&. 7) w
      where
        fmtBin 0 _ = id
        fmtBin i b = (:) (if testBit b 0 then '*' else '.') . fmtBin (i-1) (shiftR b 1)

