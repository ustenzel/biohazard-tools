{-# LANGUAGE CPP, DeriveFunctor #-}
module Dir.PTrieFinal
    ( PTrie
    , find
    , dumpToFile
    , sanityCheck
    , withFile
    ) where

import Bio.Prelude               hiding ( find )
import Bio.Util.MMap                    ( createMmapFile, mmapWithFilePtr )
import Control.Monad.Trans.State.Strict
import Dir.BitString

import qualified Bio.Streaming.Prelude         as Q
import qualified Data.ByteString               as B
import qualified Dir.PTrieStream               as S

-- | This trie no longer has stems.  Instead, the 'Split' constructor
-- stores the number of bits to skip.  Both children are wrapped in a
-- monad; the idea is that we deconde from a compact representation on
-- the fly.  This isn't suited for streaming at all.
data PTrieF  =  Tip   !Word32 [Word32]     -- payload (a non-empty list)
             |  Split !Int !PTrie !PTrie   -- split after skipping some bits


-- | Find all entries matching a prefix(!) in a trie.
-- Note the wild behavior:  this will always find something, but it's
-- nonsense if the path, which we don't have access to anymore, doesn't
-- actually match.  A short prefix will find a lot of nonsense.
find :: MonadIO m => (Int -> m ()) -> PTrie -> Bytes -> m [Word32]
find warn pt bs = go 0 pt
  where
    bitlen = 8 * B.length bs

    go i = liftIO . inspectPTrie >=> \case
        Tip   a as                     ->  pure $ a:as
        Split l u v | i + l >= bitlen  ->  merge_u <$> all_of (Just 0) u <*> all_of Nothing v
                    | hasBit (i+l) bs  ->  go (i+l+1) v
                    | otherwise        ->  go (i+l+1) u

    all_of md = liftIO . inspectPTrie >=> \case
        Tip    a as  ->  a:as <$ forM_ md warn
        Split _ u v  ->  merge_u <$> all_of (succ <$> md) u <*> all_of Nothing v


merge_u :: [Word32] -> [Word32] -> [Word32]
merge_u [] bs = bs
merge_u as [] = as
merge_u (a:as) (b:bs) | a   <   b  =  a : merge_u    as (b:bs)
                      | a   >   b  =  b : merge_u (a:as)   bs
                      | otherwise  =  a : merge_u    as    bs


{-# INLINE hasBit #-}
hasBit :: Int -> Bytes -> Bool
hasBit l bs = wi < B.length bs && testBit (B.index bs wi) bi
  where
    wi = l `shiftR` 3
    bi = 7 - (l .&. 7)


dumpToFile :: FilePath -> S.PTrie Word32 IO r -> IO ()
dumpToFile fp t = createMmapFile fp 0x7fffffff $ fmap (flip (,) ()) . execStateT (dumpPTrie 0 t)

withFile :: (MonadMask m, MonadIO m) => FilePath -> (PTrie -> m r) -> m r
withFile fn k = mmapWithFilePtr fn $ \p l -> k $ PTrie p (plusPtr p l) (plusPtr p l)

data PTrie = PTrie { rs_start, rs_ptr, rs_end :: {-# UNPACK #-} !(Ptr Word8) }
  deriving Show

type W = StateT (Ptr Word8) IO
type R = StateT PTrie IO

dumpByte :: Word8 -> W ()
dumpByte x = StateT $ \p -> flip (,) (plusPtr p 1) <$> poke p x


-- | Stores a trie compactly.  We pack stuff into bytes and use variable
-- length encodings throughout.  The layout is now backwards, because we
-- have to serialize both subtries of a Split before we can compute the
-- offset to one of them.
--
-- 0b0xxxxxxx  -- a Split with l==0 and 7-bit offset,
-- 0b100xxxxx  -- a Split with 5 bits l and variable length offset,
-- 0x101xxxxx  -- a Split with 13 bits l and variable length offset,
-- 0b11xxxxxx  -- a Tip, see below.

dumpPTrie :: Int -> S.PTrie Word32 IO r -> W r
dumpPTrie !l (S.M         m)  =  lift m >>= dumpPTrie l
dumpPTrie !l (S.Stem _ l' t)  =  dumpPTrie (l+l') t
dumpPTrie !_ (S.Tip  w ws r)  =  dumpTip Nothing w ws >> pure r
dumpPTrie !l (S.Split     x)  =  dumpSplit l x


sanityCheck :: PTrie -> S.PTrie Word32 IO r -> Q.Stream (Q.Of String) IO r
sanityCheck = go emptyBS 0
  where
    go :: BitString -> Int -> PTrie -> S.PTrie Word32 IO r -> Q.Stream (Q.Of String) IO r
    go p l ft (S.M         m)  =  lift m >>= go p l ft

    go p l ft (S.Stem k l' t)  =  let p' = appendBits p k l' in
                                  Q.cons (show l ++ " " ++ show p') $
                                  Q.zipWith (++) ("`-" `Q.cons` Q.repeat "  ") (go p' (l+l') ft t)

    go _ _ ft (S.Tip  w ws r)  =  lift (inspectPTrie ft) >>= \case
        Split m _ _                  ->  error $ printf "Split %d /= Tip %s" m (show $ w:ws)
        Tip u us | (u,us) == (w,ws)  ->  Q.cons (show (w:ws)) (pure r)
                 | otherwise         ->  error $ printf "Tip %s /= Tip %s" (show $ u:us) (show $ w:ws)

    go p l ft (S.Split     x)  =  lift (inspectPTrie ft) >>= \case
        Split m u v | l  ==   m  ->  Q.zipWith (++) ("+-" `Q.cons` Q.repeat "| ")  (go (appendBit p False) 0 u x) >>=
                                     Q.zipWith (++) ("`-" `Q.cons` Q.repeat "  ") . go (appendBit p True) 0 v
                    | otherwise  ->  error $ printf "Split %d /= Split %d" m l
        Tip w ws                 ->  error $ printf "Tip %s /= Split %d" (show $ w:ws) l


-- | Stores a 'S.Split' by first storing the left subtrie (@x@), then
-- storing the right subtrie (@y@, the result from storing @x@).  Then
-- the difference in their /end/ addresses becomes the offset, @o@, and
-- @l@ and @o@ are stored as described at 'dumpPTrie'.
dumpSplit :: Int -> S.PTrie Word32 IO (S.PTrie Word32 IO r) -> W r
dumpSplit l x = do
    y <- dumpPTrie 0 x
    px <- get
    r <- dumpPTrie 0 y
    py <- get

    case fromIntegral $ minusPtr py px of
        o | l == 0 && o < 128  ->    dumpByte $ fromIntegral o
          | l < 32             -> do dumpVarInt o
                                     dumpByte $ fromIntegral l .|.0x80
          | otherwise          -> do dumpVarInt o
                                     dumpByte . fromIntegral $ l `shiftR` 5
                                     dumpByte $ fromIntegral l .&. 0x1f .|. 0xa0
    pure r


-- | Stores a Tip, which is a list of integers.  The first (and
-- smallest) element is stored first, subsequent elements are stored as
-- differences.  The lowest bit of each integer indicates whether
-- another entry preceeds it.  The last entry has two fewer bits
-- available to allow for the tag bits as described at 'dumpPTrie'.
dumpTip :: Maybe Word32 -> Word32 -> [Word32] -> W ()
dumpTip  Nothing  w1 [     ]  =  dumpVarInt1 (shiftL   w1    1)
dumpTip (Just w0) w1 [     ]  =  dumpVarInt1 (shiftL (w1-w0) 1 .|. 1)
dumpTip  Nothing  w1 (w2:ws)  =  dumpVarInt  (shiftL   w1    1)       >> dumpTip (Just w1) w2 ws
dumpTip (Just w0) w1 (w2:ws)  =  dumpVarInt  (shiftL (w1-w0) 1 .|. 1) >> dumpTip (Just w1) w2 ws

dumpVarInt :: Word32 -> W ()
dumpVarInt w | w <  0x80  =  dumpByte (fromIntegral w)
             | otherwise  =  dumpVarInt (shiftR w 7) >> dumpByte (fromIntegral w .&. 0x7f .|. 0x80)

dumpVarInt1 :: Word32 -> W ()
dumpVarInt1 w | w <  0x20  =  dumpByte (fromIntegral w .|. 0xc0)
              | otherwise  =  dumpVarInt (shiftR w 5) >> dumpByte (fromIntegral w .&. 0x1f .|. 0xe0)


inspectPTrie :: PTrie -> IO PTrieF
inspectPTrie = evalStateT restorePTrie

-- The pointer points just past the actual encoding.
restorePTrie :: R PTrieF
restorePTrie = restoreByte >>= \case
    key | key .&. 0x80 == 0     ->  restoreSplit 0 (fromIntegral key)
        | key .&. 0xe0 == 0x80  ->  restoreVarInt >>= restoreSplit (fromIntegral key .&. 0x1f)
        | key .&. 0xe0 == 0xa0  ->  do let l_lo = key .&. 0x1f
                                       l_hi <- restoreByte
                                       let l = fromIntegral l_lo .|. shiftL (fromIntegral l_hi) 5
                                       restoreVarInt >>= restoreSplit l
        | otherwise             ->  restoreTip (key .&. 0x3f)

restoreSplit :: Int -> Int -> R PTrieF
restoreSplit l o = do
    ptr_y <- get
    let ptr_x = ptr_y { rs_ptr = plusPtr (rs_ptr ptr_y) (negate o) }
    pure $ Split l ptr_x ptr_y

restoreTip :: Word8 -> R PTrieF
restoreTip lo = do
    hi <- if lo .&. 0x20 == 0 then pure 0 else restoreVarInt
    w :!: ws <- getList $ fromIntegral (lo .&. 0x1f) .|. shiftL hi 5
    pure $! rev [] w ws
  where
    getList w | w .&. 1 == 0  =  pure $! shiftR (fromIntegral w) 1 :!: []
              | otherwise     =  do w1 :!: ws <- restoreVarInt >>= getList
                                    pure $! shiftR (fromIntegral w) 1 + w1 :!: w1:ws

    rev acc x1 [     ]  =  Tip  x1 acc
    rev acc x1 (x2:xs)  =  rev (x1:acc) x2 xs


restoreVarInt :: R Int
restoreVarInt = do
    lo <- restoreByte
    hi <- if lo .&. 0x80 == 0 then pure 0 else restoreVarInt
    pure $ fromIntegral (lo .&. 0x7f) .|. shiftL hi 7

restoreByte :: R Word8
restoreByte = StateT $ \s -> let p' = plusPtr (rs_ptr s) (-1) in
                             if rs_start s <= p' && p' < rs_end s
                             then flip (,) (s {rs_ptr = p'}) <$> peek p'
                             else error $ "Out of bounds: " ++ show s ++ ", " ++ show p'

