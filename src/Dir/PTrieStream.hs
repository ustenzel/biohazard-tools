{-# LANGUAGE CPP, DeriveFunctor #-}
{- | A PATRICIA trie intended to index BAM files by name.
 -
 - The minimalistic version without any optimizations needs about 10GB
 - of RAM to store 30M nodes.  This is obviously impractical, but it's a
 - start.
 -}
module Dir.PTrieStream
    ( PTrie(..)
    , unionPtries
    , fromAdt
    , dumpToFile
    , withFile
    , walk
    ) where

import Bio.Prelude               hiding ( find )
import Bio.Util.MMap                    ( mmapFile, createMmapFile )
import Control.Monad.Trans.State.Strict
import Dir.BitString

import qualified Bio.Streaming.Prelude as Q
import qualified Dir.PTrieAdt          as Adt

-- | We store paths up to 56 bits inline, which is a natural boundary of
-- the serialization format.  Longer paths are represented using
-- multiple 'Stem' segments.  This trie is optimized for streaming.  A
-- traversal of the on-disk format corresponds to a left-to-right
-- pre-order traversal.
-- Note the strange type of 'Split':  the right subtrie is stuck inside
-- the result value of the left subtrie.  This enforces proper
-- streaming.

data PTrie a m r  =  Stem  !Word64 !Int (PTrie a m r)             -- bits, number of bits
                  |  Tip   !a [a] r                               -- payload (a non-empty list)
                  |  Split (PTrie a m (PTrie a m r))              -- split at the first bit
                  |  M     (m (PTrie a m r))                      -- execute monadic action
  deriving ( Functor )

-- Smart constructor.  Ensures that that there is no empty 'Stem' and
-- that the unused bits of the key are zeroed out.
stem :: Word64 -> Int -> PTrie a m r -> PTrie a m r
stem _ 0 t = t
stem k l t = Stem (k .&. (complement 0 `shiftL` (64-l))) l t
{-# INLINE stem #-}

instance (Monad m, Ord a, Semigroup r) => Semigroup (PTrie a m r) where
    x <> y = uncurry (<>) <$> unionPtries x y
    {-# INLINE (<>) #-}

fromAdt :: Functor m => Adt.PTrie a -> PTrie a m ()
fromAdt (Adt.Tip   a as)  =  Tip a as ()
fromAdt (Adt.Stem k l t)  =  Stem k l (fromAdt t)
fromAdt (Adt.Split  x y)  =  Split (fromAdt y <$ fromAdt x)

unionPtries :: (Functor m, Ord a) => PTrie a m r1 -> PTrie a m r2 -> PTrie a m (r1,r2)
unionPtries             t1  (M          t2)  =  M $      unionPtries     t1 <$> t2
unionPtries (M          t1)             t2   =  M $ flip unionPtries     t2 <$> t1
unionPtries (Tip   a as r1) (Tip   b bs r2)  =  uncurry Tip (Adt.merge_u a as b bs) (r1,r2)
unionPtries (Tip   a as r1) (Split       u)  =  Split ((\(r,t) -> (,) r <$> t) <$> unionPtries (Tip a as r1) u)
unionPtries (Tip   a as r1) (Stem    k l t)
    | k == 0     =  Stem k l (unionPtries (Tip a as r1) t)
    | otherwise  =  stem 0 l1 $ Split $ Tip a as ((,) r1 <$> stem (shiftL k (l1 + 1)) (l-l1-1) t)
  where
    l1 = countLeadingZeros k

unionPtries (Split       x) (Tip   b bs r2)  =  Split ((\(t,r) -> flip (,) r <$> t) <$> unionPtries  x (Tip b bs r2))
unionPtries (Split       x) (Split       u)  =  Split (uncurry unionPtries <$> unionPtries x u)
unionPtries (Split       x) (Stem    k l t)
    | testBit k 63  =  Split $ flip unionPtries (stem (shiftL k 1) (l-1) t) <$> x
    | otherwise     =  Split $ (\(y,r2) -> flip (,) r2 <$> y) <$> unionPtries x (stem (shiftL k 1) (l-1) t)

unionPtries (Stem    k l t) (Tip   b bs r2)
    | k == 0     =  Stem k l (unionPtries t (Tip b bs r2))
    | otherwise  =  stem 0 l1 $ Split $ Tip b bs (flip (,) r2 <$> stem (shiftL k (l1 + 1)) (l-l1-1) t)
  where
    l1 = countLeadingZeros k

unionPtries (Stem    k l t) (Split       x)
    | testBit k 63  =  Split $ unionPtries (stem (shiftL k 1) (l-1) t) <$> x
    | otherwise     =  Split $ (\(r2,y) -> (,) r2 <$> y) <$> unionPtries (stem (shiftL k 1) (l-1) t) x

unionPtries (Stem k1 l1 t1) (Stem k2 l2 t2)
    | lm == 64       =  Stem k1 64 $ unionPtries t1  t2
    | lm /=  0       =  stem k1 lm $ unionPtries t1' t2'
    | testBit k1 63  =  Split $ (\r2 -> flip (,) r2 <$> stem (shiftL k1 1) (l1-1) t1) <$> stem (shiftL k2 1) (l2-1) t2
    | otherwise      =  Split $ (\r1 ->      (,) r1 <$> stem (shiftL k2 1) (l2-1) t2) <$> stem (shiftL k1 1) (l1-1) t1
  where
    lm  = countLeadingZeros (xor k1 k2) `min` l1 `min` l2
    t1' = stem (shiftL k1 lm) (l1 - lm) t1
    t2' = stem (shiftL k2 lm) (l2 - lm) t2


dumpToFile :: FilePath -> PTrie Word32 IO r -> IO r
dumpToFile fp t  =  createMmapFile fp 0x7fffffff $ \p -> uncurry (flip (,)) <$> runStateT (dumpPTrie t) p

withFile :: FilePath -> (PTrie Word32 IO () -> IO r) -> IO r
withFile fn k = do
    (_sz,fp) <- mmapFile fn
    withForeignPtr fp $ k . void . restorePTrie


type W = StateT (Ptr Word32)
type R = StateT (Ptr Word32) IO


-- Every node is stored as one Word32, possibly more:
--
-- Tip:  bit 31 clear, bit 30 set iff ws is not empty, bits 0-29 contain
--       the payload; followed by more payload in the same encoding
-- Stem:  bit 31 set, bit 30 set iff immediately followed by a Split.
--        Bits 24-29 code the length of the stem.  Bits follow (up to
--        24), followed by one more word if needed (for up to 56 bits).
-- Split:  always follows a Stem, if necessary with path length zero.
--
-- Sometimes, 'Stem' nodes are split inefficiently.  We can get 'Stem'
-- nodes with too many bits to encode, or nested 'Stem' nodes which can
-- be fused into one.  Here, we correct this implicitly.

dumpPTrie :: MonadIO m => PTrie Word32 m r -> W m r
dumpPTrie (M                   m)  =  lift m >>= dumpPTrie

dumpPTrie (Tip            w ws r)  =  r <$ dumpList w ws

dumpPTrie (Stem   k l t) | l > 56  =  do dumpStem False k 56
                                         dumpPTrie $ stem (shiftL k 56) (l-56) t

dumpPTrie (Stem    k l (M     m))  =  lift m >>= dumpPTrie . Stem k l

dumpPTrie (Stem    k l (Split x))  =  do dumpStem True k l
                                         dumpPTrie x >>= dumpPTrie

dumpPTrie (Stem k1 l1 (Stem k2 l2 t))
                  | l1 + l2 <= 56  =     dumpPTrie $ Stem (k1 .|. shiftR k2 l1) (l1+l2) t
                  | otherwise      =  do dumpStem False (k1 .|. shiftR k2 l1) 56
                                         dumpPTrie $ stem (shiftL k2 (56-l1)) (l1+l2-56) t

dumpPTrie (Stem            k l t)  =  do dumpStem False k l
                                         dumpPTrie t

dumpPTrie               (Split x)  =  do dumpInt 0xC0000000
                                         dumpPTrie x >>= dumpPTrie


dumpStem :: MonadIO m => Bool -> Word64 -> Int -> W m ()
dumpStem before_split k l = do dumpInt $ tag .|. fromIntegral (shiftL l 24) .|. fromIntegral (shiftR k 40)
                               when (l>24) $ dumpInt (fromIntegral (shiftR k 8))
  where
    tag = if before_split then 0xC0000000 else 0x80000000


dumpList :: MonadIO m => Word32 -> [Word32] -> W m ()
dumpList w ws = case filter (/= w) ws of
    [    ] -> dumpInt $ w
    w':ws' -> do dumpInt $ setBit w 30
                 dumpList w' ws'

dumpInt :: MonadIO m => Word32 -> W m ()
dumpInt x = StateT $ \p -> liftIO (poke p x) >> pure ((), plusPtr p 4)


restorePTrie :: Ptr Word32 -> PTrie Word32 IO (Ptr Word32)
restorePTrie = M . evalStateT go
  where
    go :: R (PTrie Word32 IO (Ptr Word32))
    go = do key <- restoreInt
            if testBit key 31
              then restoreStem key
              else uncurry Tip <$> restoreTip key <*> get

    restoreTip :: Word32 -> R (Word32, [Word32])
    restoreTip w = if testBit w 30 then do
                     (w',ws) <- restoreTip =<< restoreInt
                     pure (clearBit w 30, w':ws)
                   else
                     pure (w,[])

    restoreStem :: Word32 -> R (PTrie Word32 IO (Ptr Word32))
    restoreStem w = do let l = fromIntegral $ shiftR w 24 .&. 0x3F
                       let k0 = fromIntegral w .&. 0xffffff
                       k1 <- if l > 24 then restoreInt else pure 0
                       let k = shiftL k0 40 .|. shiftL (fromIntegral k1) 8
                       if testBit w 30
                         then stem k l . Split . fmap restorePTrie <$> go
                         else stem k l <$> go

    restoreInt :: R Word32
    restoreInt = StateT $ \p -> peek p >>= \x -> pure (x, plusPtr p 4)


walk :: (Monad m, Show a) => PTrie a m r -> Q.Stream (Q.Of String) m r
walk = go emptyBS
  where
    go :: (Monad m, Show a) => BitString -> PTrie a m r -> Q.Stream (Q.Of String) m r
    go p (Stem k l t) = Q.cons (show l ++ " " ++ show p') $
                        Q.zipWith (++) ("`-" `Q.cons` Q.repeat "  ") (go p' t)
        where p' = appendBits p k l

    go p (Split    x) = Q.zipWith (++) ("+-" `Q.cons` Q.repeat "| ")  (go (appendBit p False) x) >>=
                        Q.zipWith (++) ("`-" `Q.cons` Q.repeat "  ") . go (appendBit p True)

    go _ (Tip a as r) = Q.cons (show (a:as)) (pure r)

    go p (M        m) = lift m >>= go p

