Motivation
==========

Index an arbitrary (sorted, unsorted, sorted by the wrong key) bam file
/by read name/.  A possible application is to find all alignments
(secondary and supplementary) for a given read, which is currently very
awkward.

Because every read needs an entry of at least 32 bits (see below), the
index may itself be big and has to be constructed in and used from
secondary storage.  It should also be usable without having to parse it
completely.


Data Structure
--------------

Read names are frequently very repetive.  A promising index structure is
therefore a big-endian PATRICIA trie, which compresses the redundancy
away.  It's also fairly easy to merge two tries, which allows for
construction on secondary storage, effectively using a merge sort.

Indexing into bam files the samtools way (using virtual offsets) is
hopelessly inefficient, but thanks to the improperly layered codecs,
this nonsense cannot be entirely avoided.  However, we really don't want
to waste 64 bits on every single entry.

Therefore, we only store the top 30 bits (convenient for the on-disk
encoding) of the virtual offsets.  This amounts to checkpoints every
256kiB and allows for files of up to 128TiB, which matches the full
range of the virtual offsets.  Typical I/O size for actual access is
then in the 300kiB range.  On rotating disks, this isn't really worse
than the 30kiB or so achievable with a bigger index.

To retrieve data, we seek to the checkpoint, and scan forwards until we
hit a BGZF header.  Then we start decoding, and scan forwards for the
name we are trying to look up.  From there, we decode bam, filter the
desirec records, and stop once the virtual offset is past the next
checkpoint.

There is a tiny chance that the scans hit a false positive:  an
estimated 2^-35 chance for each 256kiB block to contain a false BGZF
header, and a negligible chance to hit a false qname (for a reasonable
query length).


In Practice
-----------

An incomplete, but realistic index construction, tested on a 15.7GiB bam
file, gave a 5.3Gib index (33% of the input size).  That's actually
horrible.  Of those, 26.4% are the index pointers, another 21.1% are the
internal pointers at split nodes.  The stem nodes at tips make up 35% of
the index, the remaining 17.4% are internal stem nodes.  This kind of
index uses a lot of space to store the read names, including those parts
that are common to many reads.

97% of stem nodes have a path length of 24 or less and are already
compactly encoded.  Only 24% have a length of 8 or less.  Going to a
granularity of 16 bits might make those smaller, but creates complexity.
It looks like only about 3% of the total size can be saved.  

The trie has a bit more than 2^30 entries at a depth of 41.  As
expected for a trie, that is pretty well in balance.

Split nodes could be smaller with a variable size encoding of offsets.
That only works if the file layout is backwards.  Not sure how this
would affect performance or how it could be avoided.  Assuming this
halves the space needed for internal offsets, the index file size would
shrink by just 10%.  Not worth the hassle.


New Idea
--------

To conserve space, we might simply not store the path fragments.  This
hurts lookup only in that the index is now guaranteed to produce a false
positive whenever a missing key is looked up.  However, that's not a
common use case, and the damage isn't all that big, either.  We still
need the fragments during index construction, though.

To do this cleanly, we need two different data types.  We might as well
do it right and have three:

* The index in construction.  This is an ordinary Haskell data
  structure with fragments of up to 64 bits.

* The index during merging.  This is another data type designed for
  streaming.  These can only ever be traversed in left-to-right
  pre-order.  (The right tree is the functorial value of the left tree.
  Compare the "streaming" library to understand this construction.)

* The index ready-for-use.  This doesn't have any path fragments, and
  the on-disk format uses relative pointers.  It's mmapped to memory and
  accessed by chasing pointers.
