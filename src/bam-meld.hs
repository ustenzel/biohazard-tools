{-# LANGUAGE TemplateHaskell #-}
-- Reads multiple BAM files, melds them by keeping the best hit for
-- every entry.  All input files must be parallel (same reads, same
-- order, no omissions).  The best hit and the new mapq are calculated
-- by combining appropriate optional fields.  Presets exist for common
-- types of aligners, other schemes can be configured flexibly.

import Bio.Bam
import Bio.Prelude
import Bio.Util.Git
import Paths_biohazard_tools                    ( version )
import Options.Applicative

import qualified Bio.Streaming.Prelude as Q
import qualified Data.ByteString.Char8 as S
import qualified Data.HashMap.Strict   as M

type Merge m =  Q.Stream (Q.Of [BamRec]) m ()
             -> Q.Stream (Q.Of [BamRec]) m ()
             -> Q.Stream (Q.Of [BamRec]) m ()

data Conf = Conf {
    c_output :: BamMeta -> Stream (Of BamRec) LIO () -> LIO (),
    c_merge  :: Merge LIO,
    c_score  :: BamRec -> Int,
    c_files  :: [FilePath] }

options :: Parser Conf
options = Conf
    <$> (writeBamFile <$> strOption (short 'o' <> long "output" <> help "Write output to FILE" <> metavar "FILE")
              <|> pure (\m -> pipeBamOutput m . protectTerm))
    <*> (flag' iter_transpose (short 'u' <> long "unsorted" <> help "Input is unsorted") <|>
         flag' merge_by_name  (short 's' <> long "sorted" <> help "Input is sorted by name") <|>
         pure  iter_transpose)
    <*> (fmap sum . sequence <$> some
           (option p_weight (short 'w' <> long "weight" <> metavar "XX:Y" <> help "Set the badness of field XX to Y") <|>
            flag' bwa_score (long "bwa" <> help "Preset for alignments from 'bwa' (uses XM, XO, XG)") <|>
            flag' anfo_score (long "anfo" <> help "Preset for alignments from 'anfo' (uses UQ, PQ)") <|>
            flag' blast_score (long "blast" <> help "Preset for alignments from 'blast' (uses AS)") <|>
            flag' blat_score (long "blat" <> help "Preset for alignments from 'blat' (uses NM)")) <|>
         pure bwa_score)
    <*> many (strArgument (metavar "FILE"))
  where
    bwa_score   r = 30 * extAsInt 0 "XM" r + 45 * extAsInt 0 "XO" r + 15 * extAsInt 0 "XG" r
    anfo_score  r =      extAsInt 0 "UQ" r
    blat_score  r = 30 * extAsInt 0 "NM" r
    blast_score r = -3 * extAsInt 0 "AS" r

    p_weight :: ReadM (BamRec -> Int)
    p_weight = eitherReader $ \s ->
        let err = Left $ "illegal weight specification " ++ show s
        in case s of
            (a:b:':':rest) -> case reads rest of
                [(w,"")] -> Right $ (*) w . extAsInt 0 (fromString [a,b])
                _        -> err
            _ -> err


{- | Streams a list of BAM files.

Headers are merged sensibly using the 'Semigroup' instance of 'BamMeta',
records are merged using the supplied merging function.  Importantly,
since the list of reference sequences is expected to change as a result
of merging multiple headers, the @rname@ and @mrnm@ fields in records
are updated accordingly.
-}
stream_bam_files :: (MonadIO m, MonadLog m, MonadMask m)
                 => Merge m -> [ FilePath ]
                 -> (BamMeta -> Q.Stream (Q.Of [BamRec]) m () -> m r) -> m r
stream_bam_files (~~) fs k = decodeBamFiles fs go
  where
    go [] = k mempty  (pure ())
    go bs = k hdr (foldr1 (~~) bs')
      where
        hdr = foldMap fst bs
        bs' = map (adjust hdr) bs

    adjust to (from, s) = Q.map (pure . f . unpackBam) s
      where
        !ytab = M.fromList $ toList (unRefs (meta_refs to)) `zip` [Refseq 0 ..]
        !xtab = fmap (\sq -> M.lookupDefault invalidRefseq sq ytab) (unRefs $ meta_refs from)
        xlate (Refseq rid) | rid >= fromIntegral (sizeofArray xtab)  =  invalidRefseq
                           | otherwise                               =  xtab `indexArray` fromIntegral rid
        f br = br { b_rname = xlate (b_rname br), b_mrnm  = xlate (b_mrnm br) }

data Desynchronized = Desynchronized [Bytes] deriving (Typeable, Show)
instance Exception Desynchronized where
    displayException (Desynchronized nms) =
        "BAMs are not in the same order when melding "
            ++ intercalate ", " (map unpack nms) ++ "."

meld :: MonadThrow m => BamMeta -> (BamRec -> Int) -> [BamRec] -> m (Maybe BamRec)
meld _hdr _score [      ] = pure Nothing
meld  hdr  score rs@(r:_) = case sortBy (\a b -> score a `compare` score b) $ filter (not . isUnmapped) rs of
    _ | any ((b_qname r /=) . b_qname) rs -> throwM . Desynchronized $ map b_qname rs
    [        ]                            -> pure $ Just r
    best : rs'                            -> pure $ Just new_best
      where
        mapq = case rs' of [    ] -> b_mapq best
                           (r2:_) -> Q . fromIntegral $ fromIntegral (unQ (b_mapq best))
                                                `min` (score r2 - score best)

        split_xa br = let s = extAsString "XA" br in if S.null s then id else (++) (S.split ';' s)

        xas = foldr encode (foldr split_xa [] (best:rs')) rs'

        new_best = best { b_exts = updateE "XA" (Text (S.intercalate (S.singleton ';') xas)) (b_exts best)
                        , b_mapq = mapq }

        encode b | isUnmapped b = id
                 | otherwise    = (S.intercalate (S.singleton ',') [ rnm, pos, cig, nm ] :)
          where
            nm =  fromString $ show $ extAsInt 0 "NM" b
            cig = fromString $ show $ b_cigar b
            pos = fromString $ (if isReversed b then '-' else '+') : show (b_pos b)
            rnm = sq_name $ getRef (meta_refs hdr) (b_rname b)


main :: IO ()
main =
    execWithParser_ options (Just version) (giFullVersion $$gitInfo)
                    (progDesc "Merges multiple bam files containing the same sequences, keeping only\
                               \ the best hit for each.  Attempts to be configurable to bam files from\
                               \ various sources and attempts to calculate a sensible map quality." <>
                     fullDesc) $
        \Conf{..} -> do
            add_pg <- addPG (Just version)
            stream_bam_files c_merge c_files $ \hdr ->
                c_output (add_pg hdr) . Q.concat . Q.mapM (meld hdr c_score)


data MismatchedRecords = MismatchedRecords deriving (Typeable, Show)
instance Exception MismatchedRecords where
    displayException _ = "bam files do not contain the same query records"


iter_transpose :: MonadThrow m => Merge m
iter_transpose u v = do a <- lift (Q.uncons u) ; b <- lift (Q.uncons v) ; step a b
  where
    step    Nothing         Nothing              = pure ()
    step (Just (xs, u')) (Just (ys, v'))
        | b_qname (head xs) == b_qname (head ys) = Q.cons (xs++ys) (iter_transpose u' v')
    step         _             _                 = lift . throwM $ MismatchedRecords

merge_by_name :: MonadThrow m => Merge m
merge_by_name = merge . ensure_sorting
  where
    merge   u v = do a <- lift (Q.uncons u) ; b <- lift (Q.uncons v) ; merge2 a b
    merge_x u b = do a <- lift (Q.uncons u) ; merge2 a b
    merge_y a v = do b <- lift (Q.uncons v) ; merge2 a b

    merge2  Nothing   Nothing  = pure ()
    merge2  Nothing  (Just (ys,v)) = Q.cons ys $ merge_y Nothing v
    merge2 (Just (xs,u))  Nothing  = Q.cons xs $ merge_x u Nothing
    merge2 x@(Just (xs,u)) y@(Just (ys,v)) =
        case b_qname (head xs) `compareNames` b_qname (head ys) of
            LT -> Q.cons  xs      $ merge_x u y
            EQ -> Q.cons (xs++ys) $ merge   u v
            GT -> Q.cons      ys  $ merge_y x v


data NotSortedByName = NotSortedByName deriving Typeable

instance Exception NotSortedByName
instance Show NotSortedByName where
    show _ = "input is not sorted by qname"

ensure_sorting :: MonadThrow m => Q.Stream (Q.Of [BamRec]) m r -> Q.Stream (Q.Of [BamRec]) m r
ensure_sorting = lift . Q.next >=> step
  where
    step (Left      r)      = pure r
    step (Right (x,s))      = lift (Q.next s) >>= step' x

    step' x1 (Left       r) = Q.cons x1 (pure r)
    step' x1 (Right (x2,s)) =
        case b_qname (head x1) `compareNames` b_qname (head x2) of
            GT -> lift $ throwM NotSortedByName
            _  -> Q.cons x1 $ lift (Q.next s) >>= step' x2

