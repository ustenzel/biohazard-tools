/* A sink for multiple streams.  Each file on the cmd line is opened and
 * read from in parallel, discarding the data.  Useful to test
 * bam-fixpair in multiple pipe mode.  */

#include <error.h>
#include <errno.h>
#include <alloca.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/select.h>
#include <poll.h>
#include <unistd.h>
#include <stdio.h>

int main( int argc, char**argv )
{
    struct pollfd *fds = alloca( (argc-1) * sizeof (struct pollfd) ) ;
    for( int i = 1 ; i != argc ; ++i )
    {
        fds[i-1].fd = open( argv[i], O_RDONLY ) ;
        if( fds[i-1].fd == -1 )
            error(1, errno, "failed to open %s", argv[i]) ;
        fds[i-1].events = POLLIN ;
    }

    for( int numopen = argc-1 ; numopen ; )
    {
        if( 0 > poll( fds, argc-1, -1 ) )
            error(1, errno, "poll failed" ) ;

        for( int i = 0 ; i != argc-1 ; ++i )
        {
            if( fds[i].revents & (POLLIN | POLLHUP))
            {
                char buf[4096] ;
                int s = read( fds[i].fd, buf, 4096 ) ;
                if( s < 0 )
                    error(1, errno, "read failed on %d", fds[i].fd ) ;
                if( s == 0 )
                {
                    close( fds[i].fd ) ;
                    fds[i].fd = -1 ;
                    numopen-- ;
                }
            }
        }
    }
    return 0;
}
